package com.nixsolutions.service;

import javax.servlet.http.HttpSession;

public interface LoginService {
    boolean checkSession(HttpSession session);
}
