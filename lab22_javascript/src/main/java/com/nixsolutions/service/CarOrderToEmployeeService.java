package com.nixsolutions.service;

import com.nixsolutions.model.CarOrderToEmployee;

import java.util.List;
import java.util.Map;

public interface CarOrderToEmployeeService {
    void create(Map<String, String[]> parameterMap);

    void update(Map<String, String[]> parameterMap);

    List findAll();

    Map<String, List> getCarOrdersAndEmployees();

    CarOrderToEmployee getCarOrderToEmployee(Long carOrderToEmployeeId);
}
