package com.nixsolutions.service;

import com.nixsolutions.model.EmployeeCategory;

import java.util.List;
import java.util.Map;

public interface EmployeeCategoryService {
    void create(Map<String, String[]> parameterMap);

    void update(Map<String, String[]> parameterMap);

    void delete(Long categoryId);

    List findAll();

    boolean checkCategoryToDelete(Long categoryId);

    EmployeeCategory getEmployeeCategory(Long categoryId);
}
