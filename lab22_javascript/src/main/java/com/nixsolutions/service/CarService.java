package com.nixsolutions.service;

import com.nixsolutions.model.Car;

import java.util.List;
import java.util.Map;

public interface CarService {
    void create(Map<String, String[]> parameterMap);

    void update(Map<String, String[]> parameterMap);

    void delete(Long carId);

    List findAll();

    Map<String, List> getModelsAndClients();

    Map<String, Object> getModelsClientsAndCar(Long carId);

    boolean checkCarToDelete(Long carId);

    Car getCarToDelete(Long carId);

    List findCarByNumber(String carNumber);
}
