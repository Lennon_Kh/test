package com.nixsolutions.service;

import com.nixsolutions.model.Car;
import com.nixsolutions.model.Client;

import java.util.List;
import java.util.Map;

public interface ClientService {
    void create(Map<String, String[]> parameterMap);

    void update(Map<String, String[]> parameterMap);

    void delete(Map<String, String[]> parameterMap);

    List findAll();

    Client getClient(Long clientId);

    Map getClientAndCars(Map<String, String[]> parameterMap);

    boolean checkClientCarsInOrders(List<Car> clientCars);

    List<Client> findClients(String firstName);
}
