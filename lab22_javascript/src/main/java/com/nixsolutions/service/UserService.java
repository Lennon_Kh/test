package com.nixsolutions.service;

import com.nixsolutions.model.User;
import com.nixsolutions.model.UserRole;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Map;

public interface UserService extends UserDetailsService {
    void create(Map<String, String[]> parameterMap);

    void update(Map<String, String[]> parameterMap);

    void delete(Long userId);

    User getUser(Long userId);

    List findAll();

    List<UserRole> findRoles();

    public UserDetails loadUserByUsername(String username);
}
