package com.nixsolutions.service;

import com.nixsolutions.model.CarOrder;

import java.util.List;
import java.util.Map;

public interface CarOrderService {
    void create(Map<String, String[]> parameterMap);

    void update(Map<String, String[]> parameterMap);

    void delete(Long carOrderId);

    void complete(Map<String, String[]> parameterMap);

    List findAll();

    CarOrder getCarOrder(Long carOrderId);

    Map<String, Object> getCarOrderCarsAndStatuses(Long carOrderId);

    Map<String, Object> findCarOrderByStatus(Long orderStatusId);
}
