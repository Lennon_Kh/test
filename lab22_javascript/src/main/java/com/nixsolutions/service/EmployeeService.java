package com.nixsolutions.service;

import com.nixsolutions.model.Employee;

import java.util.List;
import java.util.Map;

public interface EmployeeService {
    void create(Map<String, String[]> parameterMap);

    void update(Map<String, String[]> parameterMap);

    void delete(Long employeeId);

    List findAll();

    boolean checkEmployeeToDelete(Long employeeId);

    Employee getEmployee(Long employeeId);
}
