package com.nixsolutions.service;

import com.nixsolutions.model.CarModel;

import java.util.List;
import java.util.Map;

public interface CarModelService {
    void create(Map<String, String[]> parameterMap);

    void update(Map<String, String[]> parameterMap);

    void delete(Long modelId);

    List findAll();

    boolean checkCarModelToDelete(Long modelId);

    CarModel getCarModel(Long modelId);
}
