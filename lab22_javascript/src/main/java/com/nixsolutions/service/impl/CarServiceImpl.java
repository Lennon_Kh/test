package com.nixsolutions.service.impl;

import com.nixsolutions.dao.*;
import com.nixsolutions.model.Car;
import com.nixsolutions.model.CarModel;
import com.nixsolutions.model.CarOrder;
import com.nixsolutions.model.Client;
import com.nixsolutions.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CarServiceImpl implements CarService {
    @Autowired
    private CarDAO carDAO;
    @Autowired
    private CarModelDAO carModelDAO;
    @Autowired
    private ClientDAO clientDAO;
    @Autowired
    private CarOrderDAO carOrderDAO;

    public CarServiceImpl() {
    }

    @Override
    public void create(Map<String, String[]> parameterMap) {
        Long carModelId = Long.parseLong(parameterMap.get("carModelId")[0]);
        String description = parameterMap.get("description")[0];
        String carNumber = parameterMap.get("carNumber")[0];
        Long clientId = Long.parseLong(parameterMap.get("clientId")[0]);

        Car car = new Car();
        car.setCarModel(carModelDAO.findById(carModelId));
        car.setDescription(description);
        car.setCarNumber(carNumber);
        car.setClient(clientDAO.findById(clientId));
        carDAO.createCar(car);
    }

    @Override
    public void update(Map<String, String[]> parameterMap) {
        Long carModelId = Long.parseLong(parameterMap.get("carModelId")[0]);
        String description = parameterMap.get("description")[0];
        String carNumber = parameterMap.get("carNumber")[0];
        Long clientId = Long.parseLong(parameterMap.get("clientId")[0]);
        Long carId = Long.parseLong(parameterMap.get("carId")[0]);

        Car car = carDAO.findById(carId);
        car.setCarModel(carModelDAO.findById(carModelId));
        car.setDescription(description);
        car.setCarNumber(carNumber);
        car.setClient(clientDAO.findById(clientId));
        carDAO.updateCar(car);
    }

    @Override
    public void delete(Long carId) {
        carDAO.deleteCar(carDAO.findById(carId));
    }

    @Override
    public List findAll() {
        return carDAO.findAllIncludeClientAndModel();
    }

    @Override
    public Map<String, List> getModelsAndClients() {
        List<CarModel> models = carModelDAO.findAllOrderById();
        List<Client> clients = clientDAO.findAllOrderById();
        Map<String, List> map = new HashMap<String, List>();
        map.put("models", models);
        map.put("clients", clients);
        return map;
    }

    @Override
    public Map<String, Object> getModelsClientsAndCar(Long carId) {
        List<CarModel> models = carModelDAO.findAllOrderById();
        Car car = carDAO.findByIdIncludeClientAndModel(carId);
        List<Client> clients = clientDAO.findAllOrderById();

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("models", models);
        map.put("clients", clients);
        map.put("car", car);
        return map;
    }

    @Override
    public boolean checkCarToDelete(Long carId) {
        boolean check = false;
        Car carToDelete = carDAO.findByIdIncludeClientAndModel(carId);
        List<CarOrder> orders = carOrderDAO.findAllIncludeCar();
        for (CarOrder order : orders) {
            if (carToDelete.getCarId().equals(order.getCar().getCarId())) {
                check = true;
            }
        }
        return check;
    }

    @Override
    public Car getCarToDelete(Long carId) {
        return carDAO.findByIdIncludeClientAndModel(carId);
    }

    @Override
    public List findCarByNumber(String carNumber) {
        return carDAO.findByNumber(carNumber);
    }
}
