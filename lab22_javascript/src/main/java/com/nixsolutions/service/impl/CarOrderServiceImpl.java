package com.nixsolutions.service.impl;

import com.nixsolutions.dao.CarDAO;
import com.nixsolutions.dao.CarOrderDAO;
import com.nixsolutions.dao.OrderStatusDAO;
import com.nixsolutions.model.Car;
import com.nixsolutions.model.CarOrder;
import com.nixsolutions.model.OrderStatus;
import com.nixsolutions.service.CarOrderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CarOrderServiceImpl implements CarOrderService {
    private final static Logger logger = LogManager.getLogger();
    @Autowired
    CarOrderDAO carOrderDAO;
    @Autowired
    OrderStatusDAO orderStatusDAO;
    @Autowired
    CarDAO carDAO;

    @Override
    public void create(Map<String, String[]> parameterMap) {
        Long carId = Long.parseLong(parameterMap.get("carId")[0]);
        Double price = Double.parseDouble(parameterMap.get("price")[0]);
        Timestamp dateTimeStart = new Timestamp(new Date().getTime());

        CarOrder carOrder = new CarOrder();
        carOrder.setCar(carDAO.findById(carId));
        carOrder.setOrderStatus(orderStatusDAO.findByName("new"));
        carOrder.setDatetimeStart(dateTimeStart);
        carOrder.setPrice(new BigDecimal(price));
        carOrderDAO.createCarOrder(carOrder);
    }

    @Override
    public void update(Map<String, String[]> parameterMap) {
        Long orderId = Long.parseLong(parameterMap.get("orderId")[0]);
        Long carId = Long.parseLong(parameterMap.get("carId")[0]);
        Long orderStatusId = Long.parseLong(parameterMap.get("orderStatusId")[0]);
        Double price = Double.parseDouble(parameterMap.get("price")[0]);
        BigDecimal orderPrice = new BigDecimal(price);

        CarOrder carOrder = carOrderDAO.findById(orderId);
        carOrder.setCar(carDAO.findById(carId));
        carOrder.setOrderStatus(orderStatusDAO.findById(orderStatusId));
        carOrder.setPrice(orderPrice);

        String dateStart = parameterMap.get("datetimeStart")[0];
        try {
            Date dateSt = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(dateStart.replace("T", " "));
            Timestamp dateTimeStart = new Timestamp(dateSt.getTime());
            carOrder.setDatetimeStart(dateTimeStart);
        } catch (ParseException e) {
            logger.error("Error while parsing date");
        }

        String dateEnd = parameterMap.get("datetimeEnd")[0];
        try {
            Date dateE = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(dateEnd.replace("T", " "));
            Timestamp dateTimeEnd = new Timestamp(dateE.getTime());
            carOrder.setDatetimeEnd(dateTimeEnd);
        } catch (ParseException e) {
            carOrder.setDatetimeEnd(null);
            logger.error("Error while parsing date");
        }

        carOrderDAO.updateCarOrder(carOrder);
    }

    @Override
    public void delete(Long carOrderId) {
        CarOrder carOrder = carOrderDAO.findById(carOrderId);
        carOrderDAO.deleteCarOrder(carOrder);
    }

    @Override
    public void complete(Map<String, String[]> parameterMap) {
        Long orderId = Long.parseLong(parameterMap.get("orderId")[0]);
        Double price = Double.parseDouble(parameterMap.get("price")[0]);
        Timestamp dateTimeEnd = new Timestamp(new Date().getTime());

        CarOrder carOrder = carOrderDAO.findById(orderId);
        carOrder.setPrice(new BigDecimal(price));
        carOrder.setDatetimeEnd(dateTimeEnd);
        carOrder.setOrderStatus(orderStatusDAO.findByName("completed"));
        carOrderDAO.updateCarOrder(carOrder);
    }

    @Override
    public List findAll() {
        return carOrderDAO.findAllIncludeCarAndStatus();
    }

    @Override
    public CarOrder getCarOrder(Long carOrderId) {
        return carOrderDAO.findById(carOrderId);
    }

    @Override
    public Map<String, Object> getCarOrderCarsAndStatuses(Long carOrderId) {
        CarOrder carOrder = carOrderDAO.findByIdIncludeCarAndStatus(carOrderId);
        List<Car> cars = carDAO.findAllIncludeClientAndModel();
        List<OrderStatus> statuses = orderStatusDAO.findAllOrderById();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("carOrder", carOrder);
        map.put("cars", cars);
        map.put("statuses", statuses);
        return map;
    }

    @Override
    public Map<String, Object> findCarOrderByStatus(Long orderStatusId) {
        List<CarOrder> listOfOrders = carOrderDAO.findOrdersByStatusId(orderStatusId);
        List<OrderStatus> statuses = orderStatusDAO.findAllOrderById();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("listOfOrders", listOfOrders);
        map.put("statuses", statuses);
        return map;
    }
}
