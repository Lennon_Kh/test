package com.nixsolutions.service.impl;

import com.nixsolutions.dao.CarDAO;
import com.nixsolutions.dao.CarOrderDAO;
import com.nixsolutions.dao.ClientDAO;
import com.nixsolutions.model.Car;
import com.nixsolutions.model.CarOrder;
import com.nixsolutions.model.Client;
import com.nixsolutions.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ClientServiceImpl implements ClientService {
    @Autowired
    ClientDAO clientDAO;
    @Autowired
    CarDAO carDAO;
    @Autowired
    CarOrderDAO carOrderDAO;

    @Override
    public void create(Map<String, String[]> parameterMap) {
        String firstName = parameterMap.get("firstName")[0];
        String lastName = parameterMap.get("lastName")[0];
        String phoneNumber = parameterMap.get("phoneNumber")[0];

        Client client = new Client();
        client.setFirstName(firstName);
        client.setLastName(lastName);
        client.setPhoneNumber(phoneNumber);
        clientDAO.createClient(client);
    }

    @Override
    public void update(Map<String, String[]> parameterMap) {
        Long clientId = Long.parseLong(parameterMap.get("clientId")[0]);
        String firstName = parameterMap.get("firstName")[0];
        String lastName = parameterMap.get("lastName")[0];
        String phoneNumber = parameterMap.get("phoneNumber")[0];

        Client client = clientDAO.findById(clientId);
        client.setFirstName(firstName);
        client.setLastName(lastName);
        client.setPhoneNumber(phoneNumber);
        clientDAO.updateClient(client);
    }

    @Override
    public void delete(Map<String, String[]> parameterMap) {
        Long clientId = Long.parseLong(parameterMap.get("clientId")[0]);
        Client client = clientDAO.findById(clientId);
        List<Car> clientCars = new ArrayList<Car>();
        List<Car> cars = carDAO.findAllIncludeClientAndModel();
        for (Car car : cars) {
            if (clientId.equals(car.getClient().getClientId())) {
                clientCars.add(car);
            }
        }
        if (clientCars.size() == 0) {
            clientDAO.deleteClient(client);
        } else {
            for (Car car : clientCars) {
                carDAO.deleteCar(car);
            }
            clientDAO.deleteClient(client);
        }
    }

    @Override
    public List findAll() {
        return clientDAO.findAllOrderById();
    }

    @Override
    public Client getClient(Long clientId) {
        return clientDAO.findById(clientId);
    }

    @Override
    public Map getClientAndCars(Map<String, String[]> parameterMap) {
        Long clientId = Long.parseLong(parameterMap.get("clientId")[0]);
        Client clientToDelete = clientDAO.findById(clientId);
        List<Car> cars = carDAO.findAllIncludeClientAndModel();
        List<Car> clientCars = new ArrayList<Car>();

        for (Car car : cars) {
            if (clientId.equals(car.getClient().getClientId())) {
                clientCars.add(car);
            }
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("clientCars", clientCars);
        map.put("clientToDelete", clientToDelete);
        return map;
    }

    @Override
    public boolean checkClientCarsInOrders(List<Car> clientCars) {
        boolean check = false;
        List<Car> clientCarsToDelete = new ArrayList<Car>();
        List<CarOrder> orders = carOrderDAO.findAllIncludeCar();
        for (Car car : clientCars) {
            for (CarOrder order : orders) {
                if (car.getCarId().equals(order.getCar().getCarId())) {
                    clientCarsToDelete.add(car);
                }
            }
        }
        if (clientCarsToDelete.size() > 0) {
            check = true;
        }

        return check;
    }

    @Override
    public List<Client> findClients(String firstName) {
        return clientDAO.findByFirstName(firstName);
    }
}
