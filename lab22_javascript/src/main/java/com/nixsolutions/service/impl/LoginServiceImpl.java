package com.nixsolutions.service.impl;

import com.nixsolutions.service.LoginService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

@Service
public class LoginServiceImpl implements LoginService {
    @Override
    public boolean checkSession(HttpSession session) {
        boolean check = false;
        if (session.getAttribute("Admin") == null) {
            check = true;
        }
        return check;
    }
}
