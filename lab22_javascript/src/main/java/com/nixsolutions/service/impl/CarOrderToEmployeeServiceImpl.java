package com.nixsolutions.service.impl;

import com.nixsolutions.dao.*;
import com.nixsolutions.model.CarOrder;
import com.nixsolutions.model.CarOrderToEmployee;
import com.nixsolutions.model.CommonBean;
import com.nixsolutions.model.Employee;
import com.nixsolutions.service.CarOrderToEmployeeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CarOrderToEmployeeServiceImpl implements CarOrderToEmployeeService {
    private final static Logger logger = LogManager.getLogger();

    @Autowired
    CarOrderToEmployeeDAO carOrderToEmployeeDAO;
    @Autowired
    CarOrderDAO orderDAO;
    @Autowired
    CommonBeanDAO commonBeanDAO;
    @Autowired
    EmployeeDAO employeeDAO;
    @Autowired
    OrderStatusDAO statusDAO;

    @Override
    public void create(Map<String, String[]> parameterMap) {
        Long orderId = Long.parseLong(parameterMap.get("orderId")[0]);
        Long employeeId = Long.parseLong(parameterMap.get("employeeId")[0]);

        CarOrderToEmployee orderToEmployee = new CarOrderToEmployee();
        CarOrder carOrder = orderDAO.findByIdIncludeCarAndStatus(orderId);
        carOrder.setOrderStatus(statusDAO.findByName("picked up"));
        orderToEmployee.setCarOrder(carOrder);
        orderToEmployee.setEmployee(employeeDAO.findById(employeeId));
        orderToEmployee.setDatetimeStart(new Timestamp(new Date().getTime()));
        orderDAO.updateCarOrder(carOrder);
        carOrderToEmployeeDAO.createCarOrderToEmployee(orderToEmployee);
    }

    @Override
    public void update(Map<String, String[]> parameterMap) {
        Long employeeOrderId = Long.parseLong(parameterMap.get("employeeOrderId")[0]);
        ;
        Long orderId = Long.parseLong(parameterMap.get("orderId")[0]);
        Long employeeId = Long.parseLong(parameterMap.get("employeeId")[0]);

        CarOrderToEmployee orderToEmployee = carOrderToEmployeeDAO.findById(employeeOrderId);
        CarOrder carOrder = orderDAO.findByIdIncludeCarAndStatus(orderId);
        carOrder.setOrderStatus(statusDAO.findByName("picked up"));
        orderToEmployee.setCarOrder(carOrder);
        orderToEmployee.setEmployee(employeeDAO.findById(employeeId));

        String dateStart = parameterMap.get("datetimeStart")[0];
        try {
            Date dateSt = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(dateStart.replace("T", " "));
            Timestamp dateTimeStart = new Timestamp(dateSt.getTime());
            orderToEmployee.setDatetimeStart(dateTimeStart);
        } catch (ParseException e) {
            logger.error("Error while parsing date");
        }


        String dateEnd = parameterMap.get("datetimeEnd")[0];
        try {
            Date dateE = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(dateEnd.replace("T", " "));
            Timestamp dateTimeEnd = new Timestamp(dateE.getTime());
            orderToEmployee.setDatetimeEnd(dateTimeEnd);
        } catch (ParseException e) {
            orderToEmployee.setDatetimeEnd(null);
            logger.error("Error while parsing date");
        }


        orderDAO.updateCarOrder(carOrder);
        carOrderToEmployeeDAO.updateCarOrderToEmployee(orderToEmployee);
    }

    @Override
    public List findAll() {
        return carOrderToEmployeeDAO.findAllIncludeEmployeeAndCar();
    }

    @Override
    public Map<String, List> getCarOrdersAndEmployees() {
        List<CommonBean> carOrders = commonBeanDAO.selectAllUnfinishedOrders();
        List<Employee> employees = employeeDAO.findAllIncludeCategoryOrderById();
        Map<String, List> map = new HashMap<String, List>();
        map.put("carOrders", carOrders);
        map.put("employees", employees);
        return map;
    }

    @Override
    public CarOrderToEmployee getCarOrderToEmployee(Long carOrderToEmployeeId) {
        return carOrderToEmployeeDAO.findByIdIncludeEmployeeAndCarOrder(carOrderToEmployeeId);
    }
}
