package com.nixsolutions.service.impl;

import com.nixsolutions.dao.CommonBeanDAO;
import com.nixsolutions.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    CommonBeanDAO commonBeanDAO;

    @Override
    public List findUnfinishedOrders() {
        return commonBeanDAO.selectAllUnfinishedOrders();
    }
}
