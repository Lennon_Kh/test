package com.nixsolutions.service.impl;

import com.nixsolutions.dao.OrderStatusDAO;
import com.nixsolutions.model.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class OrderStatusServiceImpl implements com.nixsolutions.service.OrderStatusService {
    @Autowired
    OrderStatusDAO orderStatusDAO;

    @Override
    public void create(Map<String, String[]> parameterMap) {
        String name = parameterMap.get("name")[0];
        OrderStatus status = new OrderStatus();
        status.setName(name);
        orderStatusDAO.createOrderStatus(status);
    }

    @Override
    public void update(Map<String, String[]> parameterMap) {
        Long statusId = Long.parseLong(parameterMap.get("statusId")[0]);
        String name = parameterMap.get("name")[0];
        OrderStatus status = orderStatusDAO.findById(statusId);
        status.setName(name);
        orderStatusDAO.updateOrderStatus(status);
    }

    @Override
    public List findAll() {
        return orderStatusDAO.findAllOrderById();
    }

    @Override
    public OrderStatus getOrderStatus(Long statusId) {
        return orderStatusDAO.findById(statusId);
    }

}
