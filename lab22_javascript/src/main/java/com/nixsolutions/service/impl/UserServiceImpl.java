package com.nixsolutions.service.impl;

import com.nixsolutions.dao.UserDAO;
import com.nixsolutions.dao.UserRoleDAO;
import com.nixsolutions.model.User;
import com.nixsolutions.model.UserRole;
import com.nixsolutions.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserDAO userDAO;
    @Autowired
    UserRoleDAO roleDAO;

    @Override
    public void create(Map<String, String[]> parameterMap) {
        Long roleId = Long.parseLong(parameterMap.get("userRoleId")[0]);
        String login = parameterMap.get("login")[0];
        String password = parameterMap.get("password")[0];

        User user = new User();
        user.setUsername(login);
        user.setPassword(password);
        user.setUserRole(roleDAO.findById(roleId));
        userDAO.createUser(user);
    }

    @Override
    public void update(Map<String, String[]> parameterMap) {
        Long userId = Long.parseLong(parameterMap.get("userId")[0]);
        Long roleId = Long.parseLong(parameterMap.get("userRoleId")[0]);
        String login = parameterMap.get("login")[0];
        String password = parameterMap.get("password")[0];

        User user = userDAO.findById(userId);
        user.setUsername(login);
        user.setPassword(password);
        user.setUserRole(roleDAO.findById(roleId));
        userDAO.updateUser(user);
    }

    @Override
    public void delete(Long userId) {
        userDAO.deleteUser(userDAO.findById(userId));
    }

    @Override
    public User getUser(Long userId) {
        return userDAO.findById(userId);
    }

    @Override
    public List findAll() {
        return userDAO.findAllWithRolesExceptAdminOrderById();
    }

    @Override
    public List<UserRole> findRoles() {
        return roleDAO.findAllOrderById();
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userDAO.findByName(username);
        Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
        roles.add(new SimpleGrantedAuthority(user.getUserRole().getAuthority()));
        UserDetails userDetails =
                new org.springframework.security.core.userdetails
                        .User(user.getUsername(), user.getPassword(), roles);
        return userDetails;
    }


}
