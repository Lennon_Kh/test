package com.nixsolutions.service.impl;

import com.nixsolutions.dao.CarOrderToEmployeeDAO;
import com.nixsolutions.dao.EmployeeCategoryDAO;
import com.nixsolutions.dao.EmployeeDAO;
import com.nixsolutions.model.CarOrderToEmployee;
import com.nixsolutions.model.Employee;
import com.nixsolutions.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    EmployeeCategoryDAO categoryDAO;
    @Autowired
    EmployeeDAO employeeDAO;
    @Autowired
    CarOrderToEmployeeDAO carOrderToEmployeeDAO;

    @Override
    public void create(Map<String, String[]> parameterMap) {
        String firstName = parameterMap.get("firstName")[0];
        String lastName = parameterMap.get("lastName")[0];
        Long categoryId = Long.parseLong(parameterMap.get("categoryId")[0]);

        Employee employee = new Employee();
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setEmployeeCategory(categoryDAO.findById(categoryId));
        employeeDAO.createEmployee(employee);
    }

    @Override
    public void update(Map<String, String[]> parameterMap) {
        Long employeeId = Long.parseLong(parameterMap.get("employeeId")[0]);
        String firstName = parameterMap.get("firstName")[0];
        String lastName = parameterMap.get("lastName")[0];
        Long categoryId = Long.parseLong(parameterMap.get("categoryId")[0]);

        Employee employee = employeeDAO.findById(employeeId);
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setEmployeeCategory(categoryDAO.findById(categoryId));
        employeeDAO.updateEmployee(employee);
    }

    @Override
    public void delete(Long employeeId) {
        employeeDAO.deleteEmployee(employeeDAO.findById(employeeId));
    }

    @Override
    public List findAll() {
        return employeeDAO.findAllIncludeCategoryOrderById();
    }

    @Override
    public boolean checkEmployeeToDelete(Long employeeId) {
        boolean check = false;
        Employee employeeToDelete = employeeDAO.findByIdIncludeCategory(employeeId);
        List<CarOrderToEmployee> ordersToEmployee = carOrderToEmployeeDAO
                .findAllIncludeEmployeeWithCategory();
        for (CarOrderToEmployee order : ordersToEmployee) {
            if (employeeToDelete.getEmployeeId().equals(order.getEmployee().getEmployeeId())) {
                check = true;
            }
        }
        return check;
    }

    @Override
    public Employee getEmployee(Long employeeId) {
        return employeeDAO.findByIdIncludeCategory(employeeId);
    }
}
