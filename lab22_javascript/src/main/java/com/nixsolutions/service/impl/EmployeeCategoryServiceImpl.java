package com.nixsolutions.service.impl;

import com.nixsolutions.dao.EmployeeCategoryDAO;
import com.nixsolutions.dao.EmployeeDAO;
import com.nixsolutions.model.Employee;
import com.nixsolutions.model.EmployeeCategory;
import com.nixsolutions.service.EmployeeCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EmployeeCategoryServiceImpl implements EmployeeCategoryService {
    @Autowired
    private EmployeeCategoryDAO categoryDAO;
    @Autowired
    private EmployeeDAO employeeDAO;

    @Override
    public void create(Map<String, String[]> parameterMap) {
        String name = parameterMap.get("name")[0];
        EmployeeCategory category = new EmployeeCategory();
        category.setName(name);
        categoryDAO.createEmployeeCategory(category);
    }

    @Override
    public void update(Map<String, String[]> parameterMap) {
        Long categoryId = Long.parseLong(parameterMap.get("categoryId")[0]);
        String name = parameterMap.get("name")[0];
        EmployeeCategory category = categoryDAO.findById(categoryId);
        category.setName(name);
        categoryDAO.updateEmployeeCategory(category);
    }

    @Override
    public void delete(Long categoryId) {
        categoryDAO.deleteEmployeeCategory(categoryDAO.findById(categoryId));
    }

    @Override
    public List findAll() {
        return categoryDAO.findAllOrderById();
    }

    @Override
    public boolean checkCategoryToDelete(Long categoryId) {
        boolean check = false;
        EmployeeCategory categoryToDelete = categoryDAO.findById(categoryId);
        List<Employee> employees = employeeDAO.findAllIncludeCategoryOrderById();
        for (Employee employee : employees) {
            if (categoryToDelete.getEmployeeCategoryId()
                    .equals(employee.getEmployeeCategory().getEmployeeCategoryId())) {
                check = true;
            }
        }
        return check;
    }

    @Override
    public EmployeeCategory getEmployeeCategory(Long categoryId) {
        return categoryDAO.findById(categoryId);
    }
}
