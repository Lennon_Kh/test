package com.nixsolutions.service.impl;

import com.nixsolutions.dao.CarDAO;
import com.nixsolutions.dao.CarModelDAO;
import com.nixsolutions.model.Car;
import com.nixsolutions.model.CarModel;
import com.nixsolutions.service.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CarModelServiceImpl implements CarModelService {
    @Autowired
    CarModelDAO carModelDAO;
    @Autowired
    CarDAO carDAO;

    @Override
    public void create(Map<String, String[]> parameterMap) {
        String modelName = parameterMap.get("name")[0];
        CarModel model = new CarModel();
        model.setName(modelName);
        carModelDAO.createCarModel(model);
    }

    @Override
    public void update(Map<String, String[]> parameterMap) {
        Long modelId = Long.parseLong(parameterMap.get("modelId")[0]);
        String modelName = parameterMap.get("name")[0];

        CarModel model = carModelDAO.findById(modelId);
        model.setName(modelName);
        carModelDAO.updateCarModel(model);
    }

    @Override
    public void delete(Long modelId) {
        carModelDAO.deleteCarModel(carModelDAO.findById(modelId));
    }

    @Override
    public List findAll() {
        return carModelDAO.findAllOrderById();
    }

    @Override
    public boolean checkCarModelToDelete(Long modelId) {
        boolean check = false;
        CarModel modelToDelete = carModelDAO.findById(modelId);
        List<Car> cars = carDAO.findAllIncludeModelsOrderById();
        for (Car car : cars) {
            if (modelToDelete.getCarModelId().equals(car.getCarModel().getCarModelId())) {
                check = true;
            }
        }
        return check;
    }

    @Override
    public CarModel getCarModel(Long modelId) {
        return carModelDAO.findById(modelId);
    }
}
