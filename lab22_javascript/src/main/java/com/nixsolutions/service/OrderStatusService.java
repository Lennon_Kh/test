package com.nixsolutions.service;

import com.nixsolutions.model.OrderStatus;

import java.util.List;
import java.util.Map;

public interface OrderStatusService {
    void create(Map<String, String[]> parameterMap);

    void update(Map<String, String[]> parameterMap);

    List findAll();

    OrderStatus getOrderStatus(Long statusId);
}
