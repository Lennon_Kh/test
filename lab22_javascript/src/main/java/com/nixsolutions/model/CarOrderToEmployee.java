package com.nixsolutions.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
public class CarOrderToEmployee implements Serializable {

    private static final Long serialVersionUID = 1056489067L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_order_to_employee_id")
    private Long carOrderToEmployeeId;
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "car_order_id")
    private CarOrder carOrder;
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private Employee employee;
    @NotNull
    @Column(name = "datetime_start", nullable = false)
    private Timestamp datetimeStart;
    @Column(name = "datetime_end")
    private Timestamp datetimeEnd;

    public CarOrderToEmployee() {
    }

    public CarOrderToEmployee(Timestamp datetimeStart, Timestamp datetimeEnd) {
        this.datetimeStart = datetimeStart;
        this.datetimeEnd = datetimeEnd;
    }

    public Long getCarOrderToEmployeeId() {
        return carOrderToEmployeeId;
    }

    public void setCarOrderToEmployeeId(Long carOrderToEmployeeId) {
        this.carOrderToEmployeeId = carOrderToEmployeeId;
    }

    public Timestamp getDatetimeStart() {
        return datetimeStart;
    }

    public void setDatetimeStart(Timestamp datetimeStart) {
        this.datetimeStart = datetimeStart;
    }

    public Timestamp getDatetimeEnd() {
        return datetimeEnd;
    }

    public void setDatetimeEnd(Timestamp datetimeEnd) {
        this.datetimeEnd = datetimeEnd;
    }

    public CarOrder getCarOrder() {
        return carOrder;
    }

    public void setCarOrder(CarOrder carOrder) {
        this.carOrder = carOrder;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "CarOrderToEmployee{" +
                "carOrderToEmployeeId=" + carOrderToEmployeeId +
                ", datetimeStart=" + datetimeStart +
                ", datetimeEnd=" + datetimeEnd +
                '}';
    }
}
