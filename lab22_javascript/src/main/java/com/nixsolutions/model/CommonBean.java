package com.nixsolutions.model;

import java.sql.Timestamp;

public class CommonBean {
    private Long carOrderId;
    private String statusName;
    private Timestamp carOrderStartDate;
    private String carNumber;
    private String carDescription;

    public CommonBean() {
    }

    public Long getCarOrderId() {
        return carOrderId;
    }

    public void setCarOrderId(Long carOrderId) {
        this.carOrderId = carOrderId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Timestamp getCarOrderStartDate() {
        return carOrderStartDate;
    }

    public void setCarOrderStartDate(Timestamp carOrderStartDate) {
        this.carOrderStartDate = carOrderStartDate;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarDescription() {
        return carDescription;
    }

    public void setCarDescription(String carDescription) {
        this.carDescription = carDescription;
    }
}
