package com.nixsolutions.model;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class EmployeeCategory implements Serializable {

    private static final Long serialVersionUID = 1056489372L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee_category_id")
    private Long employeeCategoryId;
    @NotBlank
    @Length(max = 128)
    @Column(name = "name", nullable = false, length = 128)
    private String name;

    public EmployeeCategory() {
    }

    public EmployeeCategory(String name) {
        this.name = name;
    }

    public Long getEmployeeCategoryId() {
        return employeeCategoryId;
    }

    public void setEmployeeCategoryId(Long employeeCategoryId) {
        this.employeeCategoryId = employeeCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "EmployeeCategory{" +
                "employeeCategoryId=" + employeeCategoryId +
                ", name='" + name + '\'' +
                '}';
    }
}
