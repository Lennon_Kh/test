package com.nixsolutions.model;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class CarModel implements Serializable {

    private static final Long serialVersionUID = 1056489329L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_model_id")
    private Long carModelId;
    @NotBlank
    @Length(max = 128)
    @Column(name = "name", nullable = false, length = 128)
    private String name;

    public CarModel() {
    }

    public CarModel(String name) {
        this.name = name;
    }

    public Long getCarModelId() {
        return carModelId;
    }

    public void setCarModelId(Long carModelId) {
        this.carModelId = carModelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CarModel{" +
                "carModelId=" + carModelId +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
