package com.nixsolutions.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class CarOrder implements Serializable {

    private static final Long serialVersionUID = 1056489103L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_order_id")
    private Long carOrderId;
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "car_id")
    private Car car;
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "order_status_id")
    private OrderStatus orderStatus;
    @NotNull
    @Column(name = "datetime_start", nullable = false)
    private Timestamp datetimeStart;
    @Column(name = "datetime_end")
    private Timestamp datetimeEnd;
    @Column(name = "price", scale = 3, precision = 10)
    private BigDecimal price;

    public CarOrder() {
    }

    public CarOrder(Timestamp datetimeStart,
                    Timestamp datetimeEnd, BigDecimal price) {
        this.datetimeStart = datetimeStart;
        this.datetimeEnd = datetimeEnd;
        this.price = price;
    }

    public Long getCarOrderId() {
        return carOrderId;
    }

    public void setCarOrderId(Long carOrderId) {
        this.carOrderId = carOrderId;
    }

    public Timestamp getDatetimeStart() {
        return datetimeStart;
    }

    public void setDatetimeStart(Timestamp datetimeStart) {
        this.datetimeStart = datetimeStart;
    }

    public Timestamp getDatetimeEnd() {
        return datetimeEnd;
    }

    public void setDatetimeEnd(Timestamp datetimeEnd) {
        this.datetimeEnd = datetimeEnd;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public String toString() {
        return "CarOrder{" +
                "carOrderId=" + carOrderId +
                ", datetimeStart=" + datetimeStart +
                ", datetimeEnd=" + datetimeEnd +
                ", price=" + price +
                '}';
    }
}
