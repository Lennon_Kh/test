package com.nixsolutions.dao;

import com.nixsolutions.model.Client;

import java.util.List;

public interface ClientDAO {
    void createClient(Client client);

    void updateClient(Client client);

    boolean deleteClient(Client client);

    Client findById(Long clientId);

    List findByFirstName(String firstName);

    List<Client> findAllOrderById();
}
