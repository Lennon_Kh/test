package com.nixsolutions.dao;

import com.nixsolutions.dao.impl.H2DAOFactoryImpl;

public abstract class DAOFactory {
    public static final int H2 = 1;

    public abstract CarDAO getCarDAO();

    public abstract CarModelDAO getCarModelDAO();

    public abstract CarOrderDAO getCarOrderDAO();

    public abstract CarOrderToEmployeeDAO getCarOrderToEmployeeDAO();

    public abstract ClientDAO getClientDAO();

    public abstract EmployeeCategoryDAO getEmployeeCategoryDAO();

    public abstract EmployeeDAO getEmployeeDAO();

    public abstract OrderStatusDAO getOrderStatusDAO();

    public abstract UserDAO getUserDAO();

    public abstract UserRoleDAO getUserRoleDAO();

    public abstract CommonBeanDAO getSelectFromTableDAO();

    public static DAOFactory getDAOFactory(int whichFactory) {
        switch (whichFactory) {
            case H2:
                return new H2DAOFactoryImpl();
            default:
                return new H2DAOFactoryImpl();
        }
    }
}
