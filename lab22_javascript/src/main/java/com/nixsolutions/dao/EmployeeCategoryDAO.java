package com.nixsolutions.dao;

import com.nixsolutions.model.EmployeeCategory;

import java.util.List;

public interface EmployeeCategoryDAO {
    void createEmployeeCategory(EmployeeCategory category);

    void updateEmployeeCategory(EmployeeCategory category);

    boolean deleteEmployeeCategory(EmployeeCategory category);

    EmployeeCategory findById(Long categoryId);

    List<EmployeeCategory> findAllOrderById();
}
