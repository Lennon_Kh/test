package com.nixsolutions.dao.impl;

import com.nixsolutions.model.CarOrderToEmployee;
import com.nixsolutions.dao.CarOrderToEmployeeDAO;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class H2CarOrderToEmployeeDAOImpl implements CarOrderToEmployeeDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createCarOrderToEmployee(CarOrderToEmployee order) {
        sessionFactory.getCurrentSession().save(order);
        ;
    }

    @Override
    public void updateCarOrderToEmployee(CarOrderToEmployee order) {
        sessionFactory.getCurrentSession().update(order);
    }

    @Override
    public boolean deleteCarOrderToEmployee(CarOrderToEmployee order) {
        sessionFactory.getCurrentSession().delete(order);
        return true;
    }

    @Override
    public CarOrderToEmployee findById(Long orderId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from CarOrderToEmployee where carOrderToEmployeeId = :id ");
        query.setParameter("id", orderId);
        return (CarOrderToEmployee) query.uniqueResult();
    }

    @Override
    public CarOrderToEmployee findByIdIncludeEmployeeAndCarOrder(Long orderId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from CarOrderToEmployee where carOrderToEmployeeId = :id ");
        query.setParameter("id", orderId);
        CarOrderToEmployee order = (CarOrderToEmployee) query.uniqueResult();
        Hibernate.initialize(order.getEmployee().getEmployeeCategory());
        Hibernate.initialize(order.getCarOrder().getCar());
        return order;
    }

    @Override
    public List<CarOrderToEmployee> findAllOrderById() {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(CarOrderToEmployee.class);
        c.addOrder(Order.asc("carOrderToEmployeeId"));
        return c.list();
    }

    @Override
    public List<CarOrderToEmployee> findAllIncludeEmployeeWithCategory() {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(CarOrderToEmployee.class);
        c.addOrder(Order.asc("carOrderToEmployeeId"));
        List<CarOrderToEmployee> orders = c.list();
        List ordersList = new ArrayList<CarOrderToEmployee>();
        for (CarOrderToEmployee order : orders) {
            Hibernate.initialize(order.getEmployee().getEmployeeCategory());
            ordersList.add(order);
        }
        return ordersList;
    }

    @Override
    public List<CarOrderToEmployee> findAllIncludeEmployeeAndCar() {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(CarOrderToEmployee.class);
        c.addOrder(Order.asc("carOrderToEmployeeId"));
        List<CarOrderToEmployee> orders = c.list();
        List ordersList = new ArrayList<CarOrderToEmployee>();
        for (CarOrderToEmployee order : orders) {
            Hibernate.initialize(order.getEmployee().getEmployeeCategory());
            Hibernate.initialize(order.getCarOrder().getCar());
            Hibernate.initialize(order.getCarOrder().getOrderStatus());
            ordersList.add(order);
        }
        return ordersList;
    }
}
