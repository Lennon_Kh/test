package com.nixsolutions.dao.impl;

import com.nixsolutions.model.User;
import com.nixsolutions.dao.UserDAO;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class H2UserDAOImpl implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createUser(User user) {
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    public void updateUser(User user) {
        sessionFactory.getCurrentSession().update(user);
    }

    @Override
    public boolean deleteUser(User user) {
        sessionFactory.getCurrentSession().delete(user);
        return true;
    }

    @Override
    public User findById(Long userId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from User where userId = :id ");
        query.setParameter("id", userId);
        return (User) query.uniqueResult();
    }

    @Override
    public User findByName(String login) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from User where username = :login ");
        query.setParameter("login", login);
        return (User) query.uniqueResult();
    }

    @Override
    public List<User> findAllOrderById() {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(User.class);
        c.addOrder(Order.asc("userId"));
        return c.list();
    }

    @Override
    public List<User> findAllWithRolesExceptAdminOrderById() {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from User user where user.userRole.authority not in ('ROLE_ADMIN') ");
        return query.list();
    }
}
