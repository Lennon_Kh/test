package com.nixsolutions.dao.impl;

import com.nixsolutions.model.EmployeeCategory;
import com.nixsolutions.dao.EmployeeCategoryDAO;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class H2EmployeeCategoryDAOImpl implements EmployeeCategoryDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createEmployeeCategory(EmployeeCategory category) {
        sessionFactory.getCurrentSession().save(category);
    }

    @Override
    public void updateEmployeeCategory(EmployeeCategory category) {
        sessionFactory.getCurrentSession().update(category);
    }

    @Override
    public boolean deleteEmployeeCategory(EmployeeCategory category) {
        sessionFactory.getCurrentSession().delete(category);
        return true;
    }

    @Override
    public EmployeeCategory findById(Long categoryId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from EmployeeCategory where employeeCategoryId = :id ");
        query.setParameter("id", categoryId);
        return (EmployeeCategory) query.uniqueResult();
    }

    @Override
    public List<EmployeeCategory> findAllOrderById() {
        return sessionFactory.getCurrentSession()
                .createCriteria(EmployeeCategory.class)
                .addOrder(Order.asc("employeeCategoryId")).list();
    }
}
