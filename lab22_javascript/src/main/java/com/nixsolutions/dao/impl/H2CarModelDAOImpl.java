package com.nixsolutions.dao.impl;

import com.nixsolutions.model.CarModel;
import com.nixsolutions.dao.CarModelDAO;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class H2CarModelDAOImpl implements CarModelDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createCarModel(CarModel carModel) {
        sessionFactory.getCurrentSession().save(carModel);
    }

    @Override
    public void updateCarModel(CarModel carModel) {
        sessionFactory.getCurrentSession().update(carModel);
    }

    @Override
    public boolean deleteCarModel(CarModel carModel) {
        sessionFactory.getCurrentSession().delete(carModel);
        return true;
    }

    @Override
    public CarModel findById(Long carModelId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from CarModel where carModelId = :id ");
        query.setParameter("id", carModelId);
        return (CarModel) query.uniqueResult();
    }

    @Override
    public List<CarModel> findAllOrderById() {
        return sessionFactory.getCurrentSession()
                .createCriteria(CarModel.class)
                .addOrder(Order.asc("carModelId")).list();
    }
}
