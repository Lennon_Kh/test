package com.nixsolutions.dao.impl;

import com.nixsolutions.model.Employee;
import com.nixsolutions.dao.EmployeeDAO;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class H2EmployeeDAOImpl implements EmployeeDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createEmployee(Employee employee) {
        sessionFactory.getCurrentSession().save(employee);
    }

    @Override
    public void updateEmployee(Employee employee) {
        sessionFactory.getCurrentSession().update(employee);
    }

    @Override
    public boolean deleteEmployee(Employee employee) {
        sessionFactory.getCurrentSession().delete(employee);
        return true;
    }

    @Override
    public Employee findById(Long employeeId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from Employee where employeeId = :id ");
        query.setParameter("id", employeeId);
        return (Employee) query.uniqueResult();
    }

    @Override
    public Employee findByIdIncludeCategory(Long employeeId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from Employee where employeeId = :id ");
        query.setParameter("id", employeeId);
        Employee employee = (Employee) query.uniqueResult();
        Hibernate.initialize(employee.getEmployeeCategory());
        return employee;
    }

    @Override
    public List<Employee> findAllOrderById() {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(Employee.class);
        c.addOrder(Order.asc("employeeId"));
        return c.list();
    }

    @Override
    public List<Employee> findAllIncludeCategoryOrderById() {
        Criteria c = sessionFactory.getCurrentSession().createCriteria(Employee.class);
        c.addOrder(Order.asc("employeeId"));
        List<Employee> employees = c.list();
        List employeeList = new ArrayList<Employee>();
        for (Employee e : employees) {
            Hibernate.initialize(e.getEmployeeCategory());
            employeeList.add(e);
        }
        return employeeList;
    }
}
