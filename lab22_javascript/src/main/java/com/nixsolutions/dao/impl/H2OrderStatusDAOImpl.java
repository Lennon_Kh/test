package com.nixsolutions.dao.impl;

import com.nixsolutions.model.OrderStatus;
import com.nixsolutions.dao.OrderStatusDAO;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class H2OrderStatusDAOImpl implements OrderStatusDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createOrderStatus(OrderStatus status) {
        sessionFactory.getCurrentSession().save(status);
    }

    @Override
    public void updateOrderStatus(OrderStatus status) {
        sessionFactory.getCurrentSession().update(status);
    }

    @Override
    public boolean deleteOrderStatus(OrderStatus status) {
        sessionFactory.getCurrentSession().delete(status);
        return true;
    }

    @Override
    public OrderStatus findById(Long statusId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from OrderStatus where orderStatusId = :id ");
        query.setParameter("id", statusId);
        return (OrderStatus) query.uniqueResult();
    }

    @Override
    public OrderStatus findByName(String name) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from OrderStatus where name = :name ");
        query.setParameter("name", name);
        return (OrderStatus) query.uniqueResult();
    }

    @Override
    public List<OrderStatus> findAllOrderById() {
        return sessionFactory.getCurrentSession()
                .createCriteria(OrderStatus.class)
                .addOrder(Order.asc("orderStatusId")).list();
    }
}
