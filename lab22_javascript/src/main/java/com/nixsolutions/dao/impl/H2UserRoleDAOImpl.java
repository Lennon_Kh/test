package com.nixsolutions.dao.impl;

import com.nixsolutions.model.UserRole;
import com.nixsolutions.dao.UserRoleDAO;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class H2UserRoleDAOImpl implements UserRoleDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createUserRole(UserRole role) {
        sessionFactory.getCurrentSession().save(role);
    }

    @Override
    public void updateUserRole(UserRole role) {
        sessionFactory.getCurrentSession().update(role);
    }

    @Override
    public boolean deleteUserRole(UserRole role) {
        sessionFactory.getCurrentSession().delete(role);
        return true;
    }

    @Override
    public Long findByName(String name) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("select userRoleId from UserRole where authority = :name ");
        query.setParameter("name", name);
        return (Long) query.uniqueResult();
    }

    @Override
    public UserRole findById(Long roleId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from UserRole where userRoleId = :id ");
        query.setParameter("id", roleId);
        return (UserRole) query.uniqueResult();
    }

    @Override
    public List<UserRole> findAllOrderById() {
        return sessionFactory.getCurrentSession()
                .createCriteria(UserRole.class)
                .addOrder(Order.asc("userRoleId")).list();
    }
}
