package com.nixsolutions.dao.impl;

import com.nixsolutions.model.Car;
import com.nixsolutions.dao.CarDAO;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class H2CarDAOImpl implements CarDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createCar(Car car) {
        sessionFactory.getCurrentSession().save(car);
    }

    @Override
    public void updateCar(Car car) {
        sessionFactory.getCurrentSession().update(car);
    }

    @Override
    public boolean deleteCar(Car car) {
        sessionFactory.getCurrentSession().delete(car);
        return true;
    }

    @Override
    public Car findById(Long carId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from Car where carId = :id ");
        query.setParameter("id", carId);
        return (Car) query.uniqueResult();
    }

    @Override
    public Car findByIdIncludeClientAndModel(Long carId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from Car where carId = :id ");
        query.setParameter("id", carId);
        Car car = (Car) query.uniqueResult();
        Hibernate.initialize(car.getCarModel());
        Hibernate.initialize(car.getClient());
        return car;
    }

    @Override
    public List<Car> findByNumber(String carNumber) {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(Car.class);
        c.add(Restrictions.eq("carNumber", carNumber))
                .addOrder(Order.asc("carId"));
        List<Car> cars = c.list();
        List carsList = new ArrayList<Car>();
        for (Car car : cars) {
            Hibernate.initialize(car.getCarModel());
            Hibernate.initialize(car.getClient());
            carsList.add(car);
        }
        return cars;
    }

    @Override
    public List<Car> findAllOrderById() {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(Car.class);
        c.addOrder(Order.asc("carId"));
        return c.list();
    }

    public List<Car> findAllIncludeModelsOrderById() {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(Car.class);
        c.addOrder(Order.asc("carId"));
        List<Car> cars = c.list();
        List carsList = new ArrayList<Car>();
        for (Car car : cars) {
            Hibernate.initialize(car.getCarModel());
            carsList.add(car);
        }
        return carsList;
    }

    public List<Car> findAllIncludeClientAndModel() {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(Car.class);
        c.addOrder(Order.asc("carId"));
        List<Car> cars = c.list();
        List carsList = new ArrayList<Car>();
        for (Car car : cars) {
            Hibernate.initialize(car.getCarModel());
            Hibernate.initialize(car.getClient());
            carsList.add(car);
        }
        return carsList;
    }
}
