package com.nixsolutions.dao.impl;

import com.nixsolutions.dao.*;

public class H2DAOFactoryImpl extends DAOFactory {

    @Override
    public CarDAO getCarDAO() {
        return new H2CarDAOImpl();
    }

    @Override
    public CarModelDAO getCarModelDAO() {
        return new H2CarModelDAOImpl();
    }

    @Override
    public CarOrderDAO getCarOrderDAO() {
        return new H2CarOrderDAOImpl();
    }

    @Override
    public CarOrderToEmployeeDAO getCarOrderToEmployeeDAO() {
        return new H2CarOrderToEmployeeDAOImpl();
    }

    @Override
    public ClientDAO getClientDAO() {
        return new H2ClientDAOImpl();
    }

    @Override
    public EmployeeCategoryDAO getEmployeeCategoryDAO() {
        return new H2EmployeeCategoryDAOImpl();
    }

    @Override
    public EmployeeDAO getEmployeeDAO() {
        return new H2EmployeeDAOImpl();
    }

    @Override
    public OrderStatusDAO getOrderStatusDAO() {
        return new H2OrderStatusDAOImpl();
    }

    @Override
    public UserDAO getUserDAO() {
        return new H2UserDAOImpl();
    }

    @Override
    public UserRoleDAO getUserRoleDAO() {
        return new H2UserRoleDAOImpl();
    }

    @Override
    public CommonBeanDAO getSelectFromTableDAO() {
        return new H2CommonBeanDAOImpl();
    }


}
