package com.nixsolutions.dao.impl;

import com.nixsolutions.model.Client;
import com.nixsolutions.dao.ClientDAO;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class H2ClientDAOImpl implements ClientDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createClient(Client client) {
        sessionFactory.getCurrentSession().save(client);
    }

    @Override
    public void updateClient(Client client) {
        sessionFactory.getCurrentSession().update(client);
    }

    @Override
    public boolean deleteClient(Client client) {
        sessionFactory.getCurrentSession().delete(client);
        return true;
    }

    @Override
    public Client findById(Long clientId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from Client where clientId = :id ");
        query.setParameter("id", clientId);
        return (Client) query.uniqueResult();
    }

    @Override
    public List<Client> findByFirstName(String firstName) {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(Client.class);
        c.add(Restrictions.eq("firstName", firstName))
                .addOrder(Order.asc("clientId"));
        return c.list();
    }

    @Override
    public List<Client> findAllOrderById() {
        Criteria c = sessionFactory.getCurrentSession().createCriteria(Client.class);
        c.addOrder(Order.asc("clientId"));
        return c.list();
    }
}
