package com.nixsolutions.dao.impl;

import com.nixsolutions.model.CarOrder;
import com.nixsolutions.dao.CarOrderDAO;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class H2CarOrderDAOImpl implements CarOrderDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void createCarOrder(CarOrder carOrder) {
        sessionFactory.getCurrentSession().save(carOrder);
    }

    @Override
    public void updateCarOrder(CarOrder carOrder) {
        sessionFactory.getCurrentSession().update(carOrder);
    }

    @Override
    public boolean deleteCarOrder(CarOrder carOrder) {
        sessionFactory.getCurrentSession().delete(carOrder);
        return true;
    }

    @Override
    public CarOrder findById(Long carOrderId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from CarOrder where carOrderId = :id ");
        query.setParameter("id", carOrderId);
        return (CarOrder) query.uniqueResult();
    }

    @Override
    public CarOrder findByIdIncludeCarAndStatus(Long carOrderId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from CarOrder where carOrderId = :id ");
        query.setParameter("id", carOrderId);
        CarOrder order = (CarOrder) query.uniqueResult();
        Hibernate.initialize(order.getCar().getClient());
        Hibernate.initialize(order.getOrderStatus());
        return order;
    }

    @Override
    public List<CarOrder> findAllOrderById() {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(CarOrder.class);
        c.addOrder(Order.asc("carOrderId"));
        return c.list();
    }

    @Override
    public List<CarOrder> findAllIncludeCar() {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(CarOrder.class);
        c.addOrder(Order.asc("carOrderId"));
        List<CarOrder> orders = c.list();
        List ordersList = new ArrayList<CarOrder>();
        for (CarOrder order : orders) {
            Hibernate.initialize(order.getCar());
            ordersList.add(order);
        }
        return ordersList;
    }

    @Override
    public List<CarOrder> findAllIncludeCarAndStatus() {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(CarOrder.class);
        c.addOrder(Order.asc("carOrderId"));
        List<CarOrder> orders = c.list();
        List ordersList = new ArrayList<CarOrder>();
        for (CarOrder order : orders) {
            Hibernate.initialize(order.getCar().getClient());
            Hibernate.initialize(order.getOrderStatus());
            ordersList.add(order);
        }
        return ordersList;
    }

    @Override
    public List<CarOrder> findOrdersByStatusId(Long statusId) {
        Criteria c = sessionFactory.getCurrentSession()
                .createCriteria(CarOrder.class);
        c.add(Restrictions.eq("orderStatus.orderStatusId", statusId)).addOrder(Order.asc("carOrderId"));
        List<CarOrder> orders = c.list();
        List ordersList = new ArrayList<CarOrder>();
        for (CarOrder order : orders) {
            Hibernate.initialize(order.getCar().getClient());
            Hibernate.initialize(order.getOrderStatus());
            ordersList.add(order);
        }
        return orders;
    }
}
