package com.nixsolutions.dao.impl;

import com.nixsolutions.model.CommonBean;
import com.nixsolutions.dao.CommonBeanDAO;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class H2CommonBeanDAOImpl implements CommonBeanDAO {

    @Autowired
    private SessionFactory sessionFactory;

    /*
     * Selects all unfinished orders(where order status
     * equals 'delayed' or 'picked up'
     */
    @Override
    public List<CommonBean> selectAllUnfinishedOrders() {
        List<CommonBean> listOfUnfinishedOrders = sessionFactory.getCurrentSession()
                .createSQLQuery(
                        "SELECT o.car_order_id AS carOrderId, os.name AS statusName, " +
                                "o.datetime_start AS carOrderStartDate, " +
                                "c.car_number AS carNumber, " +
                                "c.description AS carDescription " +
                                "FROM CarOrder AS o " +
                                "RIGHT OUTER JOIN OrderStatus AS os " +
                                "ON o.order_status_id=os.order_status_id " +
                                "RIGHT OUTER JOIN Car AS c ON o.car_id=c.car_id " +
                                "WHERE os.name NOT LIKE 'completed';")
                .addScalar("carOrderId", LongType.INSTANCE)
                .addScalar("statusName", StringType.INSTANCE)
                .addScalar("carOrderStartDate", TimestampType.INSTANCE)
                .addScalar("carNumber", StringType.INSTANCE)
                .addScalar("carDescription", StringType.INSTANCE)
                .setResultTransformer(Transformers.aliasToBean(CommonBean.class))
                .list();
        return listOfUnfinishedOrders;
    }
}
