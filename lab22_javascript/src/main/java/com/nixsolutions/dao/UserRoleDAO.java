package com.nixsolutions.dao;

import com.nixsolutions.model.UserRole;

import java.util.List;

public interface UserRoleDAO {

    void createUserRole(UserRole role);

    void updateUserRole(UserRole role);

    boolean deleteUserRole(UserRole role);

    Long findByName(String name);

    UserRole findById(Long roleId);

    List<UserRole> findAllOrderById();
}
