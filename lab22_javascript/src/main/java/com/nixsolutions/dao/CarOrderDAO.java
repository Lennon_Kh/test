package com.nixsolutions.dao;

import com.nixsolutions.model.CarOrder;

import java.util.List;

public interface CarOrderDAO {
    void createCarOrder(CarOrder carOrder);

    void updateCarOrder(CarOrder carOrder);

    boolean deleteCarOrder(CarOrder carOrder);

    CarOrder findById(Long carOrderId);

    CarOrder findByIdIncludeCarAndStatus(Long carOrderId);

    List<CarOrder> findAllOrderById();

    List<CarOrder> findAllIncludeCar();

    List<CarOrder> findAllIncludeCarAndStatus();

    List<CarOrder> findOrdersByStatusId(Long statusId);
}
