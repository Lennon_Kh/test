package com.nixsolutions.dao;

import com.nixsolutions.model.CarModel;

import java.util.List;

public interface CarModelDAO {
    void createCarModel(CarModel carModel);

    void updateCarModel(CarModel carModel);

    boolean deleteCarModel(CarModel carModel);

    CarModel findById(Long carModelId);

    List<CarModel> findAllOrderById();
}
