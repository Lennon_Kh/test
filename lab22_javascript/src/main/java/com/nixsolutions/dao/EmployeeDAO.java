package com.nixsolutions.dao;

import com.nixsolutions.model.Employee;

import java.util.List;

public interface EmployeeDAO {
    void createEmployee(Employee employee);

    void updateEmployee(Employee employee);

    boolean deleteEmployee(Employee employee);

    Employee findById(Long employeeId);

    Employee findByIdIncludeCategory(Long employeeId);

    List<Employee> findAllOrderById();

    List<Employee> findAllIncludeCategoryOrderById();
}
