package com.nixsolutions.dao;

import com.nixsolutions.model.User;

import java.util.List;

public interface UserDAO {

    void createUser(User user);

    void updateUser(User user);

    boolean deleteUser(User user);

    User findById(Long userId);

    User findByName(String login);

    List<User> findAllOrderById();

    List<User> findAllWithRolesExceptAdminOrderById();
}

