package com.nixsolutions.dao;

import com.nixsolutions.model.OrderStatus;

import java.util.List;

public interface OrderStatusDAO {
    void createOrderStatus(OrderStatus status);

    void updateOrderStatus(OrderStatus status);

    boolean deleteOrderStatus(OrderStatus status);

    OrderStatus findById(Long statusId);

    OrderStatus findByName(String name);

    List<OrderStatus> findAllOrderById();
}
