package com.nixsolutions.dao;

import com.nixsolutions.model.Car;

import java.util.List;

public interface CarDAO {

    void createCar(Car car);

    void updateCar(Car car);

    boolean deleteCar(Car car);

    Car findById(Long carId);

    Car findByIdIncludeClientAndModel(Long carId);

    List<Car> findByNumber(String carNumber);

    List<Car> findAllOrderById();

    List<Car> findAllIncludeModelsOrderById();

    List<Car> findAllIncludeClientAndModel();
}
