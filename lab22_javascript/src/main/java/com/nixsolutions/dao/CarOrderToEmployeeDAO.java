package com.nixsolutions.dao;

import com.nixsolutions.model.CarOrderToEmployee;

import java.util.List;

public interface CarOrderToEmployeeDAO {
    void createCarOrderToEmployee(CarOrderToEmployee order);

    void updateCarOrderToEmployee(CarOrderToEmployee order);

    boolean deleteCarOrderToEmployee(CarOrderToEmployee order);

    CarOrderToEmployee findById(Long orderId);

    CarOrderToEmployee findByIdIncludeEmployeeAndCarOrder(Long orderId);

    List<CarOrderToEmployee> findAllOrderById();

    List<CarOrderToEmployee> findAllIncludeEmployeeWithCategory();

    List<CarOrderToEmployee> findAllIncludeEmployeeAndCar();
}
