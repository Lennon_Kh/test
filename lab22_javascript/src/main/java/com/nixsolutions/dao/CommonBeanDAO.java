package com.nixsolutions.dao;

import com.nixsolutions.model.CommonBean;

import java.util.List;

public interface CommonBeanDAO {
    List<CommonBean> selectAllUnfinishedOrders();
}
