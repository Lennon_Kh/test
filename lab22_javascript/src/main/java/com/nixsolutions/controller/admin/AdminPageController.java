package com.nixsolutions.controller.admin;

import com.nixsolutions.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AdminPageController {

    @Autowired
    private AdminService adminService;

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String listUnfinishedOrders(Model model) {
        model.addAttribute("unfinishedOrders", adminService.findUnfinishedOrders());
        return "homePage";
    }
}
