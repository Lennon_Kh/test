package com.nixsolutions.controller.order;

import com.nixsolutions.service.CarOrderService;
import com.nixsolutions.service.CarService;
import com.nixsolutions.service.OrderStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

@Controller
public class CarOrderController {

    @Autowired
    CarOrderService orderService;
    @Autowired
    CarService carService;
    @Autowired
    OrderStatusService statusService;

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String listCarOrders(Model model) {
        model.addAttribute("listOfOrders", orderService.findAll());
        return "homePage";
    }

    @RequestMapping(value = "/create_order", method = RequestMethod.GET)
    public String createCarOrderPage(Model model) {
        model.addAttribute("cars", carService.findAll());
        return "order/orders";
    }

    @RequestMapping(value = "/create_order", method = RequestMethod.POST)
    public String createCarOrder(WebRequest request) {
        orderService.create(request.getParameterMap());
        return "redirect:orders";
    }

    @RequestMapping(value = "/update_order", method = RequestMethod.GET)
    public String updateCarOrderPage(@ModelAttribute("carOrderId") Long carOrderId, Model model) {
        Map<String, Object> attributes = orderService.getCarOrderCarsAndStatuses(carOrderId);
        model.addAttribute("carOrder", attributes.get("carOrder"));
        model.addAttribute("cars", attributes.get("cars"));
        model.addAttribute("statuses", attributes.get("statuses"));
        return "order/orders";
    }

    @RequestMapping(value = "/update_order", method = RequestMethod.POST)
    public String updateCarOrder(WebRequest request) {
        orderService.update(request.getParameterMap());
        return "redirect:orders";
    }

    @RequestMapping(value = "/complete_order", method = RequestMethod.GET)
    public String completeCarOrderPage(@ModelAttribute("carOrderId") Long carOrderId, Model model) {
        model.addAttribute("completeOrder", orderService.getCarOrder(carOrderId));
        return "order/orders";
    }

    @RequestMapping(value = "/complete_order", method = RequestMethod.POST)
    public String completeCarOrder(WebRequest request) {
        orderService.complete(request.getParameterMap());
        return "redirect:orders";
    }

    @RequestMapping(value = "/delete_order", method = RequestMethod.POST)
    public String deleteCarOrder(@ModelAttribute("carOrderId") Long carOrderId) {
        orderService.delete(carOrderId);
        return "redirect:orders";
    }

    @RequestMapping(value = "/find_order_by_status", method = RequestMethod.GET)
    public String findCarOrderPage(Model model) {
        model.addAttribute("statuses", statusService.findAll());
        return "order/orderSearch";
    }

    @RequestMapping(value = "/find_order_by_status", method = RequestMethod.POST)
    public String findCarOrder(@ModelAttribute("orderStatusId") Long orderStatusId, Model model) {
        Map<String, Object> attributes = orderService.findCarOrderByStatus(orderStatusId);
        model.addAttribute("statuses", attributes.get("statuses"));
        model.addAttribute("listOfOrders", attributes.get("listOfOrders"));
        return "order/orderSearch";
    }
}
