package com.nixsolutions.controller.status;

import com.nixsolutions.service.OrderStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

@Controller
public class OrderStatusController {

    @Autowired
    private OrderStatusService statusService;

    @RequestMapping(value = "/order_statuses", method = RequestMethod.GET)
    public String listModels(Model model) {
        model.addAttribute("listOfStatus", statusService.findAll());
        return "homePage";
    }

    @RequestMapping(value = "/create_order_status", method = RequestMethod.GET)
    public String createStatusPage() {
        return "status/orderStatuses";
    }

    @RequestMapping(value = "/create_order_status", method = RequestMethod.POST)
    public String createStatus(WebRequest request) {
        statusService.create(request.getParameterMap());
        return "redirect:order_statuses";
    }

    @RequestMapping(value = "/update_status", method = RequestMethod.GET)
    public String updateStatusPage(@ModelAttribute("statusId") Long statusId, Model model) {
        model.addAttribute("status", statusService.getOrderStatus(statusId));
        return "status/orderStatuses";
    }

    @RequestMapping(value = "/update_status", method = RequestMethod.POST)
    public String updateStatus(WebRequest request) {
        statusService.update(request.getParameterMap());
        return "redirect:order_statuses";
    }
}
