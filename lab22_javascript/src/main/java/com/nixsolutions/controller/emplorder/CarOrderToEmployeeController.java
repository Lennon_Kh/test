package com.nixsolutions.controller.emplorder;

import com.nixsolutions.service.CarOrderToEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import java.util.List;
import java.util.Map;

@Controller
public class CarOrderToEmployeeController {

    @Autowired
    CarOrderToEmployeeService orderToEmployeeService;

    @RequestMapping(value = "/orders_employee", method = RequestMethod.GET)
    public String listOrdersToEmployee(Model model, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ROLE_ADMIN")) {
            model.addAttribute("listOfEmployeeOrders", orderToEmployeeService.findAll());
            return "homePage";
        } else {
            return "redirect:/user/orders_employee";
        }
    }

    @RequestMapping(value = "/user/orders_employee", method = RequestMethod.GET)
    public String listOrdersToEmployeeUser(Model model) {
        model.addAttribute("listOfEmployeeOrders", orderToEmployeeService.findAll());
        return "homePage";

    }

    @RequestMapping(value = "/create_order_for_employee", method = RequestMethod.GET)
    public String createOrderToEmployeePage(Model model) {
        Map<String, List> attributes = orderToEmployeeService.getCarOrdersAndEmployees();
        model.addAttribute("carOrders", attributes.get("carOrders"));
        model.addAttribute("employees", attributes.get("employees"));
        return "emplorder/ordersForEmp";
    }

    @RequestMapping(value = "/create_order_for_employee", method = RequestMethod.POST)
    public String createOrderToEmployee(WebRequest request) {
        orderToEmployeeService.create(request.getParameterMap());
        return "redirect:orders_employee";
    }

    @RequestMapping(value = "/update_order_for_employee", method = RequestMethod.GET)
    public String updateOrderToEmployeePage
            (@ModelAttribute("carOrderToEmployeeId") Long carOrderToEmployeeId, Model model) {
        Map<String, List> attributes = orderToEmployeeService.getCarOrdersAndEmployees();
        model.addAttribute("employeeOrder",
                orderToEmployeeService.getCarOrderToEmployee(carOrderToEmployeeId));
        model.addAttribute("carOrders", attributes.get("carOrders"));
        model.addAttribute("employees", attributes.get("employees"));
        return "emplorder/ordersForEmp";
    }

    @RequestMapping(value = "/update_order_for_employee", method = RequestMethod.POST)
    public String updateOrderToEmployee(WebRequest request) {
        orderToEmployeeService.update(request.getParameterMap());
        return "redirect:orders_employee";
    }
}
