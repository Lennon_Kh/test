package com.nixsolutions.controller.category;

import com.nixsolutions.service.EmployeeCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

@Controller
public class EmployeeCategoryController {

    @Autowired
    private EmployeeCategoryService categoryService;

    @RequestMapping(value = "/emp_categories", method = RequestMethod.GET)
    public String listCategories(Model model) {
        model.addAttribute("listOfCategories", categoryService.findAll());
        return "homePage";
    }

    @RequestMapping(value = "/create_emp_category", method = RequestMethod.GET)
    public String createCategoryPage() {
        return "category/empCategories";
    }

    @RequestMapping(value = "/create_emp_category", method = RequestMethod.POST)
    public String createCategory(WebRequest request) {
        categoryService.create(request.getParameterMap());
        return "redirect:emp_categories";
    }

    @RequestMapping(value = "/update_emp_category", method = RequestMethod.GET)
    public String updateCategoryPage(@ModelAttribute("categoryId") Long categoryId, Model model) {
        model.addAttribute("category",
                categoryService.getEmployeeCategory(categoryId));
        return "category/empCategories";
    }

    @RequestMapping(value = "/update_emp_category", method = RequestMethod.POST)
    public String updateCategory(WebRequest request) {
        categoryService.update(request.getParameterMap());
        return "redirect:emp_categories";
    }

    @RequestMapping(value = "/delete_emp_category", method = RequestMethod.GET)
    public String deleteCategoryPage(@ModelAttribute("categoryId") Long categoryId, Model model) {
        if (categoryService.checkCategoryToDelete(categoryId)) {
            return "category/deleteCategory";
        }
        model.addAttribute("categoryToDelete",
                categoryService.getEmployeeCategory(categoryId));
        return "category/deleteCategory";
    }

    @RequestMapping(value = "/delete_emp_category", method = RequestMethod.POST)
    public String deleteCategory(@ModelAttribute("categoryId") Long categoryId, Model model) {
        categoryService.delete(categoryId);
        return "redirect:emp_categories";
    }
}
