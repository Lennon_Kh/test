package com.nixsolutions.controller.user;

import com.nixsolutions.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String listUser(Model model) {
        model.addAttribute("usersAndRoles", userService.findAll());
        return "homePage";
    }

    @RequestMapping(value = "/create_user", method = RequestMethod.GET)
    public String createUserPage(Model model) {
        model.addAttribute("roles1", userService.findRoles());
        return "user/users";
    }

    @RequestMapping(value = "/create_user", method = RequestMethod.POST)
    public String createUser(WebRequest request) {
        userService.create(request.getParameterMap());
        return "redirect:users";
    }

    @RequestMapping(value = "/update_user", method = RequestMethod.GET)
    public String updateUserPage(@ModelAttribute("userId") Long userId, Model model) {
        model.addAttribute("user", userService.getUser(userId));
        model.addAttribute("roles", userService.findRoles());
        return "user/users";
    }

    @RequestMapping(value = "/update_user", method = RequestMethod.POST)
    public String updateUser(WebRequest request) {
        userService.update(request.getParameterMap());
        return "redirect:users";
    }

    @RequestMapping(value = "/delete_user", method = RequestMethod.GET)
    public String deleteUserPage(@ModelAttribute("userId") Long userId, Model model) {
        model.addAttribute("userToDelete", userService.getUser(userId));
        return "user/deleteUser";
    }

    @RequestMapping(value = "/delete_user", method = RequestMethod.POST)
    public String deleteUser(@ModelAttribute("userId") Long userId) {
        userService.delete(userId);
        return "redirect:users";
    }
}
