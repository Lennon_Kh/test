package com.nixsolutions.controller.client;

import com.nixsolutions.model.Car;
import com.nixsolutions.model.Client;
import com.nixsolutions.service.ClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import java.util.List;
import java.util.Map;

@Controller
public class ClientController {

    @Autowired
    private ClientService clientService;

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    public String listClients(Model model) {
        model.addAttribute("listOfClients", clientService.findAll());
        return "homePage";
    }

    @RequestMapping(value = "/create_client", method = RequestMethod.GET)
    public String createClientPage() {
        return "client/clients";
    }

    @RequestMapping(value = "/create_client", method = RequestMethod.POST)
    public String createClient(WebRequest request) {
        clientService.create(request.getParameterMap());
        return "redirect:clients";
    }

    @RequestMapping(value = "/update_client", method = RequestMethod.GET)
    public String updateClientPage(@ModelAttribute("clientId") Long clientId, Model model) {
        model.addAttribute("client", clientService.getClient(clientId));
        return "client/clients";
    }

    @RequestMapping(value = "/update_client", method = RequestMethod.POST)
    public String updateClient(WebRequest request) {
        clientService.update(request.getParameterMap());
        return "redirect:clients";
    }

    @RequestMapping(value = "/delete_client", method = RequestMethod.GET)
    public String deleteClientPage(WebRequest request, Model model) {
        Map attributes = clientService.getClientAndCars(request.getParameterMap());
        List<Car> clientCars = (List<Car>) attributes.get("clientCars");
        Client clientToDelete = (Client) attributes.get("clientToDelete");
        if (clientCars.size() == 0) {
            model.addAttribute("clientToDelete", clientToDelete);
            return "client/clientDelete";
        }
        if (clientService.checkClientCarsInOrders(clientCars)) {
            return "client/clientDelete";
        } else {
            model.addAttribute("clientCars", clientCars);
            model.addAttribute("clientToDelete", clientToDelete);
            return "client/clientDelete";
        }
    }

    @RequestMapping(value = "/delete_client", method = RequestMethod.POST)
    public String deleteClient(WebRequest request) {
        clientService.delete(request.getParameterMap());
        return "redirect:clients";
    }

    @RequestMapping(value = "/find_client_by_name", method = RequestMethod.GET)
    public String clientSearchPage() {
        return "client/clientSearch";
    }

    @RequestMapping(value = "/find_client_by_name", method = RequestMethod.POST)
    public String searchClient(@ModelAttribute("firstName") String firstName, Model model) {
        model.addAttribute("clientSearch", clientService.findClients(firstName));
        return "client/clientSearch";
    }
}
