package com.nixsolutions.controller.model;

import com.nixsolutions.service.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

@Controller
public class CarModelController {

    @Autowired
    private CarModelService modelService;

    @RequestMapping(value = "/car_models", method = RequestMethod.GET)
    public String listModels(Model model) {
        model.addAttribute("listOfModels", modelService.findAll());
        return "homePage";
    }

    @RequestMapping(value = "/create_car_model", method = RequestMethod.GET)
    public String createModelPage() {
        return "model/carModels";
    }

    @RequestMapping(value = "/create_car_model", method = RequestMethod.POST)
    public String createClient(WebRequest request) {
        modelService.create(request.getParameterMap());
        return "redirect:car_models";
    }

    @RequestMapping(value = "/update_model", method = RequestMethod.GET)
    public String updateClientPage(@ModelAttribute("modelId") Long modelId, Model model) {
        model.addAttribute("model", modelService.getCarModel(modelId));
        return "model/carModels";
    }

    @RequestMapping(value = "/update_model", method = RequestMethod.POST)
    public String updateClient(WebRequest request) {
        modelService.update(request.getParameterMap());
        return "redirect:car_models";
    }

    @RequestMapping(value = "/delete_model", method = RequestMethod.GET)
    public String deleteCarModelPage(@ModelAttribute("modelId") Long modelId, Model model) {
        if (modelService.checkCarModelToDelete(modelId)) {
            return "model/modelDelete";
        }
        model.addAttribute("modelToDelete", modelService.getCarModel(modelId));
        return "model/modelDelete";
    }

    @RequestMapping(value = "/delete_model", method = RequestMethod.POST)
    public String deleteCarModel(@ModelAttribute("modelId") Long modelId) {
        modelService.delete(modelId);
        return "redirect:car_models";
    }
}
