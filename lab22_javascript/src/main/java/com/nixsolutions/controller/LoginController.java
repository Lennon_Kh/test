package com.nixsolutions.controller;

import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
public class LoginController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView login(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout,
            SecurityContextHolderAwareRequestWrapper request,
            Principal user) {

        ModelAndView model = new ModelAndView();
        if (request.isUserInRole("ROLE_ADMIN")) {
            return new ModelAndView("redirect:/admin");
        } else if (request.isUserInRole("ROLE_USER")) {
            return new ModelAndView("redirect:/user/orders_employee");
        } else {
            if (error != null) {
                model.addObject("error", "Invalid username or password!");
            }

            if (logout != null) {
                model.addObject("msg", "You've been logged out successfully.");
            }
            if (user == null) {
                model.addObject("msg1", "Log in, please, to get access to pages!");
            }
            model.setViewName("../../login");
            return model;
        }
    }

}
