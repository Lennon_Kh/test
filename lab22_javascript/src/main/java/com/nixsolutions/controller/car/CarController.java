package com.nixsolutions.controller.car;

import com.nixsolutions.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

@Controller
public class CarController {

    @Autowired
    private CarService carService;

    @RequestMapping(value = "/cars", method = RequestMethod.GET)
    public String listCars(Model model) {
        model.addAttribute("listOfCars", carService.findAll());
        return "homePage";
    }

    @RequestMapping(value = "/create_car", method = RequestMethod.GET)
    public String createCarPage(Model model) {
        model.addAttribute("models", carService.getModelsAndClients().get("models"));
        model.addAttribute("clients", carService.getModelsAndClients().get("clients"));
        return "car/cars";
    }

    @RequestMapping(value = "/create_car", method = RequestMethod.POST)
    public String createCar(WebRequest request) {
        carService.create(request.getParameterMap());
        return "redirect:cars";
    }

    @RequestMapping(value = "/update_car", method = RequestMethod.GET)
    public String updateCarPage(@ModelAttribute("carId") Long carId, Model model) {
        Map<String, Object> attributes = carService.getModelsClientsAndCar(carId);
        model.addAttribute("models", attributes.get("models"));
        model.addAttribute("clients", attributes.get("clients"));
        model.addAttribute("car", attributes.get("car"));
        return "car/cars";
    }

    @RequestMapping(value = "/update_car", method = RequestMethod.POST)
    public String updateCar(WebRequest request) {
        carService.update(request.getParameterMap());
        return "redirect:cars";
    }

    @RequestMapping(value = "/delete_car", method = RequestMethod.GET)
    public String deleteCarPage(@ModelAttribute("carId") Long carId, Model model) {
        if (carService.checkCarToDelete(carId)) {
            return "car/carDelete";
        }
        model.addAttribute("carToDelete", carService.getCarToDelete(carId));
        return "car/carDelete";
    }

    @RequestMapping(value = "/delete_car", method = RequestMethod.POST)
    public String deleteCar(@ModelAttribute("carId") Long carId) {
        carService.delete(carId);
        return "redirect:cars";
    }

    @RequestMapping(value = "/find_car_by_number", method = RequestMethod.GET)
    public String findCarByNumberPage() {
        return "car/carSearch";
    }

    @RequestMapping(value = "/find_car_by_number", method = RequestMethod.POST)
    public String findCarByNumber(@ModelAttribute("carNumber") String carNumber, Model model) {
        model.addAttribute("carSearch", carService.findCarByNumber(carNumber));
        return "car/carSearch";
    }
}
