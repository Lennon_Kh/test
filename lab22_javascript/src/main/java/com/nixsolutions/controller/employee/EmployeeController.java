package com.nixsolutions.controller.employee;

import com.nixsolutions.service.EmployeeCategoryService;
import com.nixsolutions.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

@Controller
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeCategoryService categoryService;

    @RequestMapping(value = "/employees", method = RequestMethod.GET)
    public String listEmployees(Model model) {
        model.addAttribute("listOfEmployees", employeeService.findAll());
        return "homePage";
    }

    @RequestMapping(value = "/create_employee", method = RequestMethod.GET)
    public String createEmployeePage(Model model) {
        model.addAttribute("categories", categoryService.findAll());
        return "employee/employees";
    }

    @RequestMapping(value = "/create_employee", method = RequestMethod.POST)
    public String createEmployee(WebRequest request) {
        employeeService.create(request.getParameterMap());
        return "redirect:employees";
    }

    @RequestMapping(value = "/update_employee", method = RequestMethod.GET)
    public String updateEmployeePage(@ModelAttribute("employeeId") Long employeeId, Model model) {
        model.addAttribute("employee", employeeService.getEmployee(employeeId));
        model.addAttribute("categories", categoryService.findAll());
        return "employee/employees";
    }

    @RequestMapping(value = "/update_employee", method = RequestMethod.POST)
    public String updateEmployee(WebRequest request) {
        employeeService.update(request.getParameterMap());
        return "redirect:employees";
    }

    @RequestMapping(value = "/delete_employee", method = RequestMethod.GET)
    public String deleteEmployeePage(@ModelAttribute("employeeId") Long employeeId, Model model) {
        if (employeeService.checkEmployeeToDelete(employeeId)) {
            return "employee/deleteEmployee";
        }
        model.addAttribute("employeeToDelete",
                employeeService.getEmployee(employeeId));
        return "employee/deleteEmployee";
    }

    @RequestMapping(value = "/delete_employee", method = RequestMethod.POST)
    public String deleteEmployee(@ModelAttribute("employeeId") Long employeeId) {
        employeeService.delete(employeeId);
        return "redirect:employees";
    }
}
