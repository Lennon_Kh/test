INSERT INTO EmployeeCategory (name) VALUES ('mechanic');

INSERT INTO EmployeeCategory (name) VALUES ( 'electrician');

INSERT INTO EmployeeCategory (name) VALUES ( 'lube technician');

INSERT INTO EmployeeCategory (name) VALUES ( 'engine mechanic');

INSERT INTO EmployeeCategory (name) VALUES ( 'transmission mechanic');

INSERT INTO EmployeeCategory (name) VALUES ( 'wheel alignment and brake mechanic');

INSERT INTO Employee (first_name, last_name, employee_category_id) VALUES ('John', 'Lennon', 1);

INSERT INTO Employee (first_name, last_name, employee_category_id) VALUES ('Paul', 'McCartney', 2);

INSERT INTO Employee (first_name, last_name, employee_category_id) VALUES ('Ringo', 'Starr', 3);

INSERT INTO Employee (first_name, last_name, employee_category_id) VALUES ('George', 'Harrison', 4);

INSERT INTO Employee (first_name, last_name, employee_category_id) VALUES ('Anthony', 'Kiedis', 5);

INSERT INTO Employee (first_name, last_name, employee_category_id) VALUES ('Michael', 'Balzary', 6);

INSERT INTO Employee (first_name, last_name, employee_category_id) VALUES ('Robert', 'Plant', 2);

INSERT INTO CarModel (name) VALUES ('Volkswagen Polo');

INSERT INTO CarModel (name) VALUES ('Audi A6');

INSERT INTO CarModel (name) VALUES ('Pegout 3008');

INSERT INTO CarModel (name) VALUES ('BMV X5');

INSERT INTO CarModel (name) VALUES ('Range Rover Sport');

INSERT INTO CarModel (name) VALUES ('Volkswagen Cross Polo');

INSERT INTO Client (first_name, last_name, phone_number) VALUES ('Mick', 'Jagger', '08007777700');

INSERT INTO Client (first_name, last_name, phone_number) VALUES ('Jimmy', 'Page', '08006556655');

INSERT INTO Client (first_name, last_name, phone_number) VALUES ('John', 'Bonem', '0800');

UPDATE Client SET phone_number = '08005000500' WHERE client_id = 3;

INSERT INTO Client (first_name, last_name, phone_number) VALUES ('John', 'Frusciante', '08001110333');

INSERT INTO Client (first_name, last_name, phone_number) VALUES ('unknown', 'unknown', '08005000500');

INSERT INTO OrderStatus (name) VALUES ('delayed');

INSERT INTO OrderStatus (name) VALUES ('completed');

INSERT INTO OrderStatus (name) VALUES ('picked up');

INSERT INTO OrderStatus (name) VALUES ('new');

INSERT INTO Car (car_model_id, description, car_number, client_id) VALUES (5, 'Replace the oil filter, tires', 'JK35672L', 1);

INSERT INTO Car (car_model_id, description, car_number, client_id) VALUES (6, 'Tune the engine', 'FW84208Q', 3);

INSERT INTO Car (car_model_id, description, car_number, client_id) VALUES (2, 'Replace the spark plugs', 'VC35410D', 2);

INSERT INTO Car (car_model_id, description, car_number, client_id) VALUES (3, 'Paint wheels', 'VC82180D', 1);

INSERT INTO Car (car_model_id, description, car_number, client_id) VALUES (1, 'Check tires', 'VC09630D', 3);

INSERT INTO CarOrder (car_id, order_status_id, datetime_start, price) VALUES (1, 1, '2015-07-25 11:00:52', 400.57);

INSERT INTO CarOrder (car_id, order_status_id, datetime_start, price) VALUES (2, 1, '2015-07-24 13:42:01', 1700.95);

UPDATE CarOrder SET order_status_id = 3 WHERE car_id = 2;

INSERT INTO CarOrder (car_id, order_status_id, datetime_start, price) VALUES (3, 1, '2015-07-24 10:05:11', 600.00);

UPDATE CarOrder SET order_status_id = 2 WHERE car_id = 3;

UPDATE CarOrder SET datetime_end = '2015-07-25 17:08:19' WHERE car_id = 3;

INSERT INTO CarOrderToEmployee (car_order_id, employee_id, datetime_start) VALUES (1, 3, '2015-07-25 11:30:00');

INSERT INTO CarOrderToEmployee (car_order_id, employee_id, datetime_start) VALUES (1, 6, '2015-07-25 11:30:00');

INSERT INTO CarOrderToEmployee (car_order_id, employee_id, datetime_start) VALUES (2, 4, '2015-07-24 14:00:00');

INSERT INTO CarOrderToEmployee (car_order_id, employee_id, datetime_start) VALUES (3, 1, '2015-07-24 11:00:00');

UPDATE CarOrderToEmployee SET datetime_end = '2015-07-25 17:00:00' WHERE car_order_id = 3;

INSERT INTO UserRole (authority) VALUES ('ROLE_ADMIN');

INSERT INTO UserRole (authority) VALUES ('ROLE_USER');

INSERT INTO User (username, password, user_role_id) VALUES ('admin', 'admin', 1);

INSERT INTO User (username, password, user_role_id) VALUES ('user', 'user', 2);

