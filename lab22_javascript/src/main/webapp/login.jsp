<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Login</title>
    <script src="<c:url value="/resources/js/showHintForInputField.js"/>" ></script>
    <script src="<c:url value="/resources/js/formValidation.js"/>" ></script>
    <link href="<c:url value="/resources/style.css" />" rel="stylesheet">
</head>
<body>
    <div id="header">
        <h1>Car Service Station</h1>
    </div>
    <div id="section">
        <c:if test="${not empty error}">
            <div>${error}</div>
        </c:if>
        <c:if test="${not empty msg}">
            <div>${msg}</div>
        </c:if>
        <c:if test="${not empty msg1}">
            <div>${msg1}</div>
        </c:if>
        </br>
    <form name="formLogin" action="<c:url value="/j_spring_security_check"></c:url>" method="POST" onsubmit="return validate(this, {j_username : [REQUIRED_VALIDATOR, ALPHANUMERIC_VALIDATOR], j_password : [REQUIRED_VALIDATOR, ALPHANUMERIC_VALIDATOR]})">
        <table width="100%">
        <tr><td>Username:</tr></td>
        <tr><td><input type="text" name="j_username" size="30" maxlength="64">
        <span id="hint">Enter your valid username to login</span></br></tr></td>
        <tr><td>Password:</tr></td>
        <tr><td><input type="password" name="j_password" size="30"  maxlength="64">
        <span id="hint">Enter your valid password to login</span></br></br></tr></td>
        <tr><td><input type="submit" value="Log in"></tr></td>
        </table>
    </form>
    </div>
</body>
</html>