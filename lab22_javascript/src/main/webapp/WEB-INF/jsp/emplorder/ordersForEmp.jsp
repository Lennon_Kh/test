<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<c:choose>
  <c:when test="${not empty requestScope.employeeOrder}" >
    <title>Updating order for employee</title>
  </c:when>
  <c:when test="${empty requestScope.employeeOrder}" >
    <title>Creating order for employee</title>
  </c:when>
</c:choose>
<script src="<c:url value="/resources/js/showHintForInputField.js"/>" ></script>
<script src="<c:url value="/resources/js/formValidation.js"/>" ></script>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <a href="./create_order_for_employee">
    Add order for employee
  </a>
</div>
<div id="section">
  <c:choose>
    <c:when test="${not empty requestScope.employeeOrder}" >
      <form action="./update_order_for_employee" method="POST" onsubmit="return validate(this, {datetimeStart : [REQUIRED_VALIDATOR, CURRENT_DATE_VALIDATOR], datetimeEnd : [COfE_END_DATE_VALIDATOR]})">
        <fieldset>
          <legend>Edit order for employee information : </legend>
          <table width="100%" cellspacing="0" cellpadding="4">
            <input type="hidden" name="employeeOrderId"
         value="${employeeOrder.carOrderToEmployeeId}">
              <td width="40%"></br>
            <tr>
              <td align="right">Order id and car number : </td>
              <td width="60%">
                  <select name="orderId">
                    <option value="${employeeOrder.carOrder.carOrderId}" selected>
                    Order id : ${employeeOrder.carOrder.carOrderId},
                    Car number : ${employeeOrder.carOrder.car.carNumber}</option>
                    <c:if test="${not empty requestScope.carOrders}">
                    <c:forEach var="carOrders" items="${requestScope.carOrders}">
                      <option value="${carOrders.carOrderId}">Order id : ${carOrders.carOrderId},
                      Car number : ${carOrders.carNumber}</option>
                    </c:forEach>
                </c:if>
                </select>
                <span id="hint">Choose the car from car order for repair</span>
                </br>
                </br></td>
            </tr>
            <tr>
              <td align="right">Employee : </td>
              <td><c:if test="${not empty requestScope.employees}">
                  <select name="employeeId">
                    <option value="${employeeOrder.employee.employeeId}"
                selected>${employeeOrder.employee.lastName},
                ${employeeOrder.employee.employeeCategory.name}</option>
                    <c:forEach var="employees" items="${requestScope.employees}">
                      <option value="${employees.employeeId}">${employees.lastName},
                      ${employees.employeeCategory.name}</option>
                    </c:forEach>
                  </select>
                </c:if>
                <span id="hint">Choose the the employee</br>
                 which will repair the car</span>
                </br>
                </br></td>
            </tr>
            <tr>
              <td align="right">Order registration date : </br>
              Enter date in format like: YYYY-MM-DD hh:mm</td>
              <td><fmt:formatDate value="${employeeOrder.datetimeStart}"
                                         type="date"
                                         pattern="yyyy-MM-dd'T'hh:mm"
                                          var="formattedDateStart" />
              <input type="datetime-local" name="datetimeStart" value="${formattedDateStart}" size="30">
        		<span id="hint">Edit the date when employee</br>
        		began the repair of the car</span>
                </br>
                </br></td>
            </tr>
            <tr>
              <td align="right">Order completion date : </br>
                Enter date in format like: YYYY-MM-DD hh:mm</td>
              <td><fmt:formatDate value="${employeeOrder.datetimeEnd}"
                                         type="date"
                                         pattern="yyyy-MM-dd'T'hh:mm"
                                         var="formattedDateEnd" />
              <input type="datetime-local" name="datetimeEnd" value="${formattedDateEnd}" size="30">
         		<span id="hint">Edit the date when employee</br>
                        		finished the repair of the car</span>
                </br>
                </br></td>
            </tr>
            <tr>
              <td></td>
              <td><input type="submit" value="Update"></td>
            </tr>
          </table>
        </fieldset>
      </form>
    </c:when>
    <c:when test="${empty requestScope.employeeOrder}" >
      <form action="./create_order_for_employee" method="POST">
        <fieldset>
          <legend>Enter information about order for employee : </legend>
          <table width="100%" cellspacing="0" cellpadding="4">
            <tr>
              <td width="40%" align="right">Order id and car number : </td>
              <td width="60%"><c:if test="${not empty requestScope.carOrders}">
                  <select name="orderId">
                    <c:forEach var="carOrders" items="${requestScope.carOrders}">
                      <option value="${carOrders.carOrderId}">Order id : ${carOrders.carOrderId},
                      Car number : ${carOrders.carNumber}</option>
                    </c:forEach>
                  </select>
                  <span id="hint">Choose the car from car order for repair</span>
                </c:if>
                <c:if test="${empty requestScope.carOrders}">
                 There are no available unfinished orders
                </c:if>
                </br>
                </br></td>
            </tr>
            <tr>
              <td align="right">Employee : </td>
              <td><c:if test="${not empty requestScope.employees}">
                  <select name="employeeId">
                    <c:forEach var="employees" items="${requestScope.employees}">
                      <option value="${employees.employeeId}">${employees.lastName},
                      ${employees.employeeCategory.name}</option>
                    </c:forEach>
                  </select>
                </c:if>
                <span id="hint">Choose the the employee</br>
                                 which will repair the car</span>
                </br>
                </br></td>
            </tr>
            <tr>
              <td></td>
              <td>
              <c:if test="${not empty requestScope.carOrders}">
              <input type="submit" value="Create">
              </c:if>
              <c:if test="${empty requestScope.carOrders}">
              <a href="./orders_employee">Return</a>
              </c:if></td>
            </tr>
          </table>
        </fieldset>
      </form>
    </c:when>
  </c:choose>
</div>
</body>
</html>