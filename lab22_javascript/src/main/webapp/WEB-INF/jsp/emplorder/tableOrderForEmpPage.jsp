<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
Orders for employee <br>
<br>
<table id="table" border=1 align=center>
  <tr>
    <th>Employee order id</th>
    <th>Car order status</th>
    <th>Car number</th>
    <th>Description</th>
    <th>Employee</th>
    <th>Category</th>
    <th>Start date</th>
    <th>Completion date</th>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
    <th>Action</th>
    </sec:authorize>
    <c:forEach var="listOfEmployeeOrders" items="${requestScope.listOfEmployeeOrders}">
        <tr>
          <td>${listOfEmployeeOrders.carOrderToEmployeeId}</td>
          <td>${listOfEmployeeOrders.carOrder.orderStatus.name}</td>
          <td>${listOfEmployeeOrders.carOrder.car.carNumber}</td>
          <td class="td">${listOfEmployeeOrders.carOrder.car.description}</td>
          <td class="td">${listOfEmployeeOrders.employee.firstName}
          ${listOfEmployeeOrders.employee.lastName}</td>
          <td class="td">${listOfEmployeeOrders.employee.employeeCategory.name}</td>
          <td>${listOfEmployeeOrders.datetimeStart}</td>
          <td>${listOfEmployeeOrders.datetimeEnd}</td>
          <sec:authorize access="hasRole('ROLE_ADMIN')">
          <td><form action="./update_order_for_employee" method="get">
              <input type="hidden" name="carOrderToEmployeeId"
                        value="${listOfEmployeeOrders.carOrderToEmployeeId}">
              <input type="submit" value="Edit"/>
            </form></td>
          </sec:authorize>
        </tr>
    </c:forEach>
</table>
