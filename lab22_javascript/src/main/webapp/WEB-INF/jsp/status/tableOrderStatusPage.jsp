<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>

Order statuses <br>
<br>
<table id="table" border=1 align=center>
  <tr>
    <th>Status id</th>
    <th>Name</th>
    <th>Action</th>
    <c:forEach var="listOfStatus" items="${requestScope.listOfStatus}">      
        <c:choose>
            <c:when test=
            "${(listOfStatus.name eq 'new') || (listOfStatus.name eq 'completed')|| (listOfStatus.name eq 'picked up')}">
            </c:when>
            <c:otherwise>
                <tr>
                    <td>${listOfStatus.orderStatusId}</td>
                    <td class="td">${listOfStatus.name}</td>
                    <td><form action="./update_status" method="get">
                    <input type="hidden" name="statusId" value="${listOfStatus.orderStatusId}">
                    <input type="submit" value="Edit"/>
                    </form></td>
                </tr>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</table>




