<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
<c:if test="${not empty requestScope.status}">
  <title>Updating order status</title>
</c:if>
<c:if test="${empty requestScope.status}">
  <title>Creating order status</title>
</c:if>
<script src="<c:url value="/resources/js/showHintForInputField.js"/>" ></script>
<script src="<c:url value="/resources/js/formValidation.js"/>" ></script>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <a href="./create_order_status">
    Add order status
  </a>
</div>
<div id="section">
  <c:if test="${not empty requestScope.status}">
    <form action="./update_status" method="POST" onsubmit="return validate(this, {name : [REQUIRED_VALIDATOR, LETTERS_VALIDATOR]})">
      <fieldset>
        <legend>Edit status information : </legend>
        <table width="100%" cellspacing="0" cellpadding="4">
          <input type="hidden" name="statusId" value="${status.orderStatusId}">
          <tr>
            <td width="40%" align="right">Status name : </td>
            <td width="60%"><input type="text" name="name" value="${status.name}" size="30" maxlength="256">
                <span id="hint">Edit the name of the order status</span>
              </br>
              </br></td>
          </tr>
          <tr>
            <td></td>
            <td><input type="submit" value="Update"></td>
          </tr>
        </table>
      </fieldset>
    </form>
  </c:if>
  <c:if test="${empty requestScope.status}">
    <form action="./create_order_status" method="POST" onsubmit="return validate(this, {name : [REQUIRED_VALIDATOR, LETTERS_VALIDATOR]})">
      <fieldset>
        <legend>Enter information about status : </legend>
        <table width="100%" cellspacing="0" cellpadding="4">
          <tr>
            <td width="40%" align="right">Status name : </td>
            <td width="60%"><input type="text" name="name" value="" size="30" maxlength="256">
                    <span id="hint">Enter a name of new order status</span>
              </br>
              </br></td>
          </tr>
          <tr>
            <td></td>
            <td><input type="submit" value="Create"></td>
          </tr>
        </table>
      </fieldset>
    </form>
  </c:if>
</div>
</body>
</html>