<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
<title>Order search</title>
<script src="<c:url value="/resources/js/showHintForInputField.js"/>" ></script>
<script src="<c:url value="/resources/js/highlightTableRows.js"/>" ></script>
<style type="text/css">
  .clickedRow { background-color: #c1d879; }
</style>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <a href="./create_order">
    Add order
  </a>
  <a href="./find_order_by_status">
    Find order by status
  </a>
</div>
<div id="section"> Select order status : </br>
  </br>
  <form action="./find_order_by_status" method="POST">
    <select name="orderStatusId">
      <c:forEach var="statuses" items="${requestScope.statuses}">
        <option value="${statuses.orderStatusId}">${statuses.name}</option>
      </c:forEach>
    </select>
    <span id="hint">Choose order status to find</br>
    all car orders with this status</span>
    </br>
    </br>
    <input type="submit" value="Search">
  </form>
  <br>
  <br>
  <c:if test="${not empty requestScope.listOfOrders}">
    <jsp:include page="tableOrderPage.jsp" flush="true" />
  </c:if>
  <script type="text/javascript">
    highlightTableRows("table", "clickedRow");
  </script>
</div>
</body>
</html>