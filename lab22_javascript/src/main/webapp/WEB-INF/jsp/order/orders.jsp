<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<c:choose>
  <c:when test="${not empty requestScope.carOrder}">
    <title>Updating car order</title>
  </c:when>
  <c:when test="${empty requestScope.completeOrder}">
    <title>Completing car order</title>
  </c:when>
  <c:when test="${empty requestScope.carOrder}">
    <title>Creating car order</title>
  </c:when>
</c:choose>
<script src="<c:url value="/resources/js/showHintForInputField.js"/>" ></script>
<script src="<c:url value="/resources/js/formValidation.js"/>" ></script>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <a href="./create_order">
    Add order
  </a>
  <a href="./find_order_by_status">
    Find order by status
  </a>
</div>
<div id="section">
  <c:choose>
    <c:when test="${not empty requestScope.carOrder}" >
      <form action="./update_order" method="POST" onsubmit="return validate(this, {datetimeStart : [REQUIRED_VALIDATOR, CURRENT_DATE_VALIDATOR], datetimeEnd : [END_DATE_VALIDATOR], price : [REQUIRED_VALIDATOR]})">
        <fieldset>
          <legend>Edit order information : </legend>
          <table width="100%" cellspacing="0" cellpadding="4">
            <input type="hidden" name="orderId" value="${carOrder.carOrderId}">
              </br>            
            <tr>
              <td width="40%" align="right">Client & car number : </td>
              <td width="60%"><c:if test="${not empty requestScope.cars}">
                  <select name="carId">
                      <option value="${carOrder.car.carId}" selected>${carOrder.car.client.lastName}
                       ${carOrder.car.carNumber}</option>
                    <c:forEach var="cars" items="${requestScope.cars}">
                      <option value="${cars.carId}">${cars.client.lastName}
                      ${cars.carNumber}</option>
                    </c:forEach>
                  </select>
                </c:if>
                <span id="hint">Choose the car for car order</br>
                 by clients last name and car number</span>
                </br></td>
            </tr>
            <tr>
              <td align="right">Order status : </td>

              <td><c:if test="${not empty requestScope.statuses}">
                  <select name="orderStatusId">
                      <option value="${carOrder.orderStatus.orderStatusId}"
                 		selected>${carOrder.orderStatus.name}</option>
                    <c:forEach var="statuses" items="${requestScope.statuses}">
                      <option value="${statuses.orderStatusId}">${statuses.name}</option>
                    </c:forEach>
                  </select>
                </c:if>
                <span id="hint">Choose the current status of the order</span>
                </br></td>
            </tr>
            <tr>
              <td align="right">Order registration date : </br>
                Enter date in format like: YYYY-MM-DD hh:mm</td>
              <td><fmt:formatDate value="${carOrder.datetimeStart}"
                                        type="date"
                                        pattern="yyyy-MM-dd'T'hh:mm"
                                        var="formattedDateStart" />
                <input type="datetime-local" name="datetimeStart"
        		value="${formattedDateStart}" size="25">
                <span id="hint">Edit the date when the order for car repair</br>
                was registered in car service</span>
                 </br></td>
            </tr>
            <tr>
              <td align="right">Order completion date : </br>
                Enter date in format like: YYYY-MM-DD hh:mm</td>
              <td><fmt:formatDate value="${carOrder.datetimeEnd}"
                                type="date"
                                pattern="yyyy-MM-dd'T'hh:mm"
                                var="formattedDateEnd" />
              <input type="datetime-local" name="datetimeEnd"
               value="${formattedDateEnd}" size="25">
                <span id="hint">Edit the date when the order for car repair</br>
                 was completed</span>
                </br></br>
                </td>
            </tr>
            <tr>
              <td align="right">Price : </td>
              <td><input type="number" name="price" value="${carOrder.price}"
            max="10000000000" step="0.1" value="3" size="30">
                <span id="hint">Edit the price of the order</span>
                </br>
                </td>
            </tr>
            <tr>
              <td></td>
              <td><input type="submit" value="Update"></td>
            </tr>
          </table>
        </fieldset>
      </form>
    </c:when>
    <c:when test="${not empty requestScope.completeOrder}">
      <form action="./complete_order" method="POST" onsubmit="return validate(this, {price : [REQUIRED_VALIDATOR, PRICE_VALIDATOR]})">
        <fieldset>
          <legend>Edit order information : </legend>
          <table width="100%" cellspacing="0" cellpadding="4">
            <input type="hidden" name="orderId" value="${completeOrder.carOrderId}">
              </br>
            <tr>
              <td width="40%" align="right">Price : </td>
              <td width="60%" ><input type="number" name="price" value="${completeOrder.price}"
                      max="10000000000" step="0.1" value="3" size="30">
                      <span id="hint">Enter or edit the price of the order</span>
                </br></td>
            </tr>
            <tr>
              <td></td>
              <td><input type="submit" value="Complete"></td>
            </tr>
          </table>
        </fieldset>
      </form>
    </c:when>
    <c:when test="${empty requestScope.carOrder}" >
      <form action="./create_order" method="POST" onsubmit="return validate(this, {price : [REQUIRED_VALIDATOR]})">
        <fieldset>
          <legend>Enter information about order : </legend>
          <table width="100%" cellspacing="0" cellpadding="4">
            <tr>
              <td width="40%" align="right">Client & car number : </td>
              <td width="60%"><c:if test="${not empty requestScope.cars}">
                  <select name="carId">
                    <c:forEach var="cars" items="${requestScope.cars}">
                      <option value="${cars.carId}">${cars.client.lastName}
                      ${cars.carNumber}</option>
                    </c:forEach>
                  </select>
                </c:if>
                <span id="hint">Choose the car for car order</br>
                                 by clients last name and car number</span>
                </br></td>
            </tr>
            <tr>
              <td align="right">Price : </td>
              <td><input type="number" name="price" value="0.000" size="30"
                  max="10000000000" step="0.1" value="3">
                  <span id="hint">Enter the price of the order</span>
                </br></td>
            </tr>
            <tr>
              <td></td>
              <td><input type="submit" value="Create"></td>
            </tr>
          </table>
        </fieldset>
      </form>
    </c:when>
  </c:choose>
</div>
</body>
</html>