<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>

Orders <br>
<br>
<table id="table" border=1 align=center>
  <tr>
    <th>Order id</th>
    <th>Status</th>
    <th>Order date</th>
    <th>Car number</th>
    <th>Description</th>
    <th>Client</th>
    <th>Completion date</th>
    <th>Price</th>
    <th>Action</th>
    <c:forEach var="listOfOrders" items="${requestScope.listOfOrders}">      
        <tr>
          <td>${listOfOrders.carOrderId}</td>
          <td class="td">${listOfOrders.orderStatus.name}</td>
          <td>${listOfOrders.datetimeStart}</td>
          <td>${listOfOrders.car.carNumber}</td>
          <td class="td">${listOfOrders.car.description}</td>
          <td class="td">${listOfOrders.car.client.firstName}
                         ${listOfOrders.car.client.lastName}</td>
          <td>${listOfOrders.datetimeEnd}</td>
          <td>${listOfOrders.price}</td>
          <td><form action="./update_order" method="get">
              <input type="hidden" name="carOrderId" value="${listOfOrders.carOrderId}">
              <input type="submit" value="Edit"/>
            </form>
            <c:if test="${listOfOrders.orderStatus.name eq 'new'}">
              <form action="./delete_order" method="post">
                <input type="hidden" name="carOrderId" value="${listOfOrders.carOrderId}">
                <input type="submit" value="Delete"/>
              </form>
            </c:if>
            <c:if test="${listOfOrders.orderStatus.name ne 'completed'}">
              <form action="./complete_order" method="get">
                <input type="hidden" name="carOrderId" value="${listOfOrders.carOrderId}">
                <input type="submit" value="Complete"/>
              </form>
            </c:if></td>
        </tr>
    </c:forEach>
</table>