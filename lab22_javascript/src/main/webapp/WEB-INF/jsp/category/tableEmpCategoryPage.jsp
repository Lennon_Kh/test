<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>

Employee categories <br>
<br>
<table id="table" border=1 align=center>
  <tr>
    <th>Category id</th>
    <th>Name</th>
    <th>Action</th>
    <c:forEach var="listOfCategories" items="${requestScope.listOfCategories}">      
        <tr>
          <td>${listOfCategories.employeeCategoryId}</td>
          <td class="td">${listOfCategories.name}</td>
          <td><form action="./update_emp_category" method="get">
              <input type="hidden" name="categoryId" value="${listOfCategories.employeeCategoryId}">
              <input type="submit" value="Edit"/>
            </form>
              <form action="./delete_emp_category" method="get">
              <input type="hidden" name="categoryId" value="${listOfCategories.employeeCategoryId}">
              <input type="submit" value="Delete"/>
            </form></td>
        </tr>
    </c:forEach>
</table>
