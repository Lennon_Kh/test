<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
    <title>Deleting employee category</title>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/resources/js/highlightTableRows.js"/>" ></script>
<style type="text/css">
  .clickedRow { background-color: #c1d879; }
</style>
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <c:if test="${not empty requestScope.categoryToDelete}"> Are you sure you want to delete
   employee category ?</c:if>
</div>
<div id="section">
<c:if test="${empty requestScope.categoryToDelete}"> Employee category cant be deleted because
    it is used for description of employee(s) !</br>
    </br>
    <a href="./emp_categories">
      Return to employee categories page
    </a>
  </c:if>
  <c:if test="${not empty requestScope.categoryToDelete}">
      <table id="table" border=1 align=center>
        <tr>
          <th>Category id</th>
          <th>Name</th>
        <tr>
          <td>${categoryToDelete.employeeCategoryId}</td>
          <td class="td">${categoryToDelete.name}</td>
        </tr>
      </table>
    </c:if>
    </br>
    <table width="100%" cellspacing="0" cellpadding="4">
    <tr>
      <td align="right" width="50%">
      <c:if test="${not empty requestScope.categoryToDelete}">
        <form method="POST" action="./delete_emp_category">
          <input type="hidden" name="categoryId" value="${categoryToDelete.employeeCategoryId}">
          <input type="submit" value="Delete">
        </form>
      </c:if></td>
      <td width="50%">
      <c:if test="${not empty requestScope.categoryToDelete}">
        <form action="./emp_categories">
          <input type="submit" value="Return">
        </form>
      </c:if></td>
    </tr>
  </table>
   <script type="text/javascript">
     highlightTableRows("table", "clickedRow");
   </script>
</div>
</body>
</html>