<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
<script src="<c:url value="/resources/js/showHintForInputField.js"/>" ></script>
<script src="<c:url value="/resources/js/formValidation.js"/>" ></script>
<c:if test="${not empty requestScope.category}">
  <title>Updating employee category</title>
</c:if>
<c:if test="${empty requestScope.category}">
  <title>Creating employee category</title>
</c:if>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <a href="./create_emp_category">
    Add employee category
  </a>
</div>
<div id="section">
  <c:if test="${not empty requestScope.category}">
    <form action="./update_emp_category" method="POST" onsubmit="return validate(this, {name : [REQUIRED_VALIDATOR, LETTERS_VALIDATOR]})">
      <fieldset>
        <legend>Edit category information : </legend>
        <table width="100%" cellspacing="0" cellpadding="4">
          <input type="hidden" name="categoryId" value="${category.employeeCategoryId}">
            <td width="40%"></br>          
          <tr>
            <td align="right">Name : </td>
            <td width="60%"><input type="text" name="name" value="${category.name}" size="30" maxlength="128">
                <span id="hint">Edit the name of the employee category</span>
              </td>
          </tr>
          <tr>
            <td></td>
            <td><input type="submit" value="Update"></td>
          </tr>
        </table>
      </fieldset>
    </form>
  </c:if>
  <c:if test="${empty requestScope.category}">
    <form action="./create_emp_category" method="POST" onsubmit="return validate(this, {name : [REQUIRED_VALIDATOR, LETTERS_VALIDATOR]})">
      <fieldset>
        <legend>Enter information about category : </legend>
        <table width="100%" cellspacing="0" cellpadding="4">
          <tr>
            <td width="40%" align="right">Category name : </td>
            <td width="60%"><input type="text" name="name" value="" size="30" maxlength="128">
                    <span id="hint">Enter a name of a new employee category</span>
             </td>
          </tr>
          <tr>
            <td></td>
            <td><input type="submit" value="Create"></td>
          </tr>
        </table>
      </fieldset>
    </form>
  </c:if>
</div>
</body>
</html>