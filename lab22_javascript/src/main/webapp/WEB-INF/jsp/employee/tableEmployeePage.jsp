<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>

Employees <br>
<br>
<table id="table" border=1 align=center>
  <tr>
    <th>Employee id</th>
    <th>First name</th>
    <th>Last name</th>
    <th>Category name</th>
    <th>Action</th>
    <c:forEach var="listOfEmployees" items="${requestScope.listOfEmployees}">      
        <tr>
          <td>${listOfEmployees.employeeId}</td>
          <td class="td">${listOfEmployees.firstName}</td>
          <td class="td">${listOfEmployees.lastName}</td>
          <td class="td">${listOfEmployees.employeeCategory.name}</td>
          <td><form action="./update_employee" method="get">
              <input type="hidden" name="employeeId" value="${listOfEmployees.employeeId}">
              <input type="submit" value="Edit"/>
            </form>
              <form action="./delete_employee" method="get">
              <input type="hidden" name="employeeId" value="${listOfEmployees.employeeId}">
              <input type="submit" value="Delete"/>
            </form></td>
        </tr>
    </c:forEach>
</table>
