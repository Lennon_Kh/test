<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
<c:choose>
  <c:when test="${not empty requestScope.employee}" >
    <title>Updating employee</title>
  </c:when>
  <c:when test="${empty requestScope.employee}" >
    <title>Creating employee</title>
  </c:when>
</c:choose>
<script src="<c:url value="/resources/js/showHintForInputField.js"/>" ></script>
<script src="<c:url value="/resources/js/formValidation.js"/>" ></script>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <a href="./create_employee">
    Add employee
  </a>
</div>
<div id="section">
  <c:choose>
    <c:when test="${not empty requestScope.employee}" >
      <form action="./update_employee" method="POST" onsubmit="return validate(this, {firstName : [REQUIRED_VALIDATOR, LETTERS_VALIDATOR], lastName : [REQUIRED_VALIDATOR, LETTERS_VALIDATOR]})">
        <fieldset>
          <legend>Edit employee information : </legend>
          <table width="100%" cellspacing="0" cellpadding="4">
            <input type="hidden" name="employeeId" value="${employee.employeeId}">
            <td width="40%">  </br>            
            <tr>
              <td align="right">First name : </td>
              <td width="60%"><input type="text" name="firstName" value="${employee.firstName}" size="30"  maxlength="64">
                      <span id="hint">Edit the first name of the employee</span>
                </td>
            </tr>
            <tr>
              <td align="right">Last name : </td>
              <td><input type="text" name="lastName" value="${employee.lastName}" size="30"  maxlength="64">
                      <span id="hint">Edit the last name of the employee</span>
                </td>
            </tr>
            <tr>
              <td align="right">Category: </td>
              <td><c:if test="${not empty requestScope.categories}">
                  <select name="categoryId">
                      <option value="${employee.employeeCategory.employeeCategoryId}"
                                   selected>${employee.employeeCategory.name}</option>
                    <c:forEach var="categories" items="${requestScope.categories}">
                      <option value="${categories.employeeCategoryId}">${categories.name}</option>
                    </c:forEach>
                  </select>
                </c:if>
                <span id="hint">Choose the category of the employee</span>
                </td>
            </tr>
            <tr>
              <td></td>
              <td><input type="submit" value="Update"></td>
            </tr>
          </table>
        </fieldset>
      </form>
    </c:when>
    <c:when test="${empty requestScope.employee}" >
      <form action="./create_employee" method="POST" onsubmit="return validate(this, {firstName : [REQUIRED_VALIDATOR, LETTERS_VALIDATOR], lastName : [REQUIRED_VALIDATOR, LETTERS_VALIDATOR]})">
        <fieldset>
          <legend>Enter information about employee : </legend>
          <table width="100%" cellspacing="0" cellpadding="4">
            <tr>
              <td width="40%" align="right">First name : </td>
              <td width="60%"><input type="text" name="firstName" value="" size="30"  maxlength="64">
                        <span id="hint">Enter the first name of new employee</span>
                </td>
            </tr>
            <tr>
              <td align="right">Last name : </td>
              <td><input type="text" name="lastName" value="" size="30"  maxlength="64">
                        <span id="hint">Enter the last name of new employee</span>
                </td>
            </tr>
            <tr>
              <td align="right">Category: </td>
              <td><c:if test="${not empty requestScope.categories}">
                  <select name="categoryId">
                    <c:forEach var="categories" items="${requestScope.categories}">
                      <option value="${categories.employeeCategoryId}">${categories.name}</option>
                    </c:forEach>
                  </select>
                </c:if>
                <span id="hint">Choose the category of new employee</span>
                </td>
            </tr>
            <tr>
              <td></td>
              <td><input type="submit" value="Create"></td>
            </tr>
          </table>
        </fieldset>
      </form>
    </c:when>
  </c:choose>
</div>
</body>
</html>