<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
    <title>Deleting employee</title>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/resources/js/highlightTableRows.js"/>" ></script>
<style type="text/css">
  .clickedRow { background-color: #c1d879; }
</style>
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <c:if test="${not empty requestScope.employeeToDelete}"> Are you sure you want to delete
   employee ?</c:if>
</div>
<div id="section">
<c:if test="${empty requestScope.employeeToDelete}"> Employee cant be deleted because
    he was added to order(s) for employee !</br>
    </br>
    <a href="./employees">
      Return to employees page
    </a>
  </c:if>
  <c:if test="${not empty requestScope.employeeToDelete}">
      <table id="table" border=1 align=center>
        <tr>
          <th>Employee id</th>
              <th>First name</th>
              <th>Last name</th>
              <th>Category name</th>
        <tr>
          <td>${employeeToDelete.employeeId}</td>
          <td class="td">${employeeToDelete.firstName}</td>
          <td class="td">${employeeToDelete.lastName}</td>
          <td>${employeeToDelete.employeeCategory.name}</td>
        </tr>
      </table>
    </c:if>
    </br>
    <table width="100%" cellspacing="0" cellpadding="4">
    <tr>
      <td align="right" width="50%"><c:if test="${not empty requestScope.employeeToDelete}">
        <form method="POST" action="./delete_employee">
          <input type="hidden" name="employeeId" value="${employeeToDelete.employeeId}">
          <input type="submit" value="Delete">
        </form>
      </c:if></td>
      <td width="50%"><c:if test="${not empty requestScope.employeeToDelete}">
        <form action="./employees">          
          <input type="submit" value="Return">
        </form>
      </c:if></td>
    </tr>
  </table>
  <script type="text/javascript">
    highlightTableRows("table", "clickedRow");
  </script>
</div>
</body>
</html>