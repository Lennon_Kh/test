<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>

Clients <br>
<br>
<table id="table" border=1 align=center>
  <tr>
    <th>Client id</th>
    <th>First name</th>
    <th>Last name</th>
    <th>Phone number</th>
    <th>Action</th>
    <c:forEach var="listOfClients" items="${requestScope.listOfClients}">      
        <tr>
          <td>${listOfClients.clientId}</td>
          <td class="td">${listOfClients.firstName}</td>
          <td class="td">${listOfClients.lastName}</td>
          <td class="td">${listOfClients.phoneNumber}</td>
          <td><form action="./update_client" method="get">
              <input type="hidden" name="clientId" value="${listOfClients.clientId}">
              <input type="submit" value="Edit"/>
            </form>
              <form action="./delete_client" method="get">
              <input type="hidden" name="clientId" value="${listOfClients.clientId}">
              <input type="submit" value="Delete"/>
            </form></td>
        </tr>
    </c:forEach>
</table>
