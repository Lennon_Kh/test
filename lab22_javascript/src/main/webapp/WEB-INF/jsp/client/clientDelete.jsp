<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
<title>Deleting client</title>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/resources/js/highlightTableRows.js"/>" ></script>
<style type="text/css">
  .clickedRow { background-color: #c1d879; }
</style>
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <c:if test="${not empty requestScope.clientToDelete}"> Are you sure you want to delete client </c:if>
  <c:if test="${not empty requestScope.clientCars}"> and all his cars </c:if>
</div>
<div id="section">
  <c:if test="${empty requestScope.clientToDelete}"> Client cant be deleted because he has car(s)
    which were added to car orders for repairing ! </br>
    </br>
    <a href="./clients">
      Return to clients page
    </a>
  </c:if>
  <c:if test="${not empty requestScope.clientToDelete}">
    <table id="table" border=1 align=center>
      <tr>
        <th>Client id</th>
        <th>First name</th>
        <th>Last name</th>
        <th>Phone number</th>
      <tr>
        <td>${clientToDelete.clientId}</td>
        <td class="td">${clientToDelete.firstName}</td>
        <td class="td">${clientToDelete.lastName}</td>
        <td class="td">${clientToDelete.phoneNumber}</td>
      </tr>
    </table>
  </c:if>
  </br>
  </br>
  <c:if test="${not empty requestScope.clientCars}">
    <table id="table" border=1 align=center>
      <tr>
        <th>Car id</th>
        <th>Car model</th>
        <th>Description</th>
        <th>Car number</th>
        <th>Client</th>
        <c:forEach var="clientCars" items="${requestScope.clientCars}">
          
            <tr>
              <td>${clientCars.carId}</td>
              <td class="td">${clientCars.carModel.name}</td>
              <td class="td">${clientCars.description}</td>
              <td>${clientCars.carNumber}</td>
              <td class="td">${clientCars.client.firstName}
                ${clientCars.client.lastName}</td>
            </tr>
        </c:forEach>
    </table>
  </c:if>
  </br>
  <table width="100%" cellspacing="0" cellpadding="4">
    <tr>
      <td align="right" width="50%"><c:if test="${not empty requestScope.clientToDelete}">
          <form method="POST" action="./delete_client">
            <input type="hidden" name="clientId" value="${clientToDelete.clientId}">
            <input type="submit" value="Delete">
          </form>
        </c:if></td>
      <td width="50%"><c:if test="${not empty requestScope.clientToDelete}">
          <form action="./clients">
            <input type="submit" value="Return">
          </form>
        </c:if></td>
    </tr>
  </table>
  <script type="text/javascript">
    highlightTableRows("table", "clickedRow");
  </script>
</div>
</body>
</html>