<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<title>Client search</title>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<script src="<c:url value="/resources/js/showHintForInputField.js"/>" ></script>
<script src="<c:url value="/resources/js/highlightTableRows.js"/>" ></script>
<script src="<c:url value="/resources/js/formValidation.js"/>" ></script>
<style type="text/css">
  .clickedRow { background-color: #c1d879; }
</style>
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <a href="./create_client">
    Add client
  </a>
  <a href="./find_client_by_name">
    Find client by name
  </a>
</div>
<div id="section"> Enter clients first name : </br>
  </br>
  <form action="./find_client_by_name" method="POST" onsubmit="return validate(this, {firstName : [REQUIRED_VALIDATOR, LETTERS_VALIDATOR]})">
    First name :
    <input type="text" name="firstName" value="" size="30" maxlength="64">
                    <span id="hint">Enter full first name of the client</br>
                    which you want to find</span>
    </br>
    </br>
    <input type="submit" value="Search">
  </form>
  <c:if test="${not empty requestScope.clientSearch}">
    <br>
    <br>
    <table id="table" border=1 align=center>
      <tr>
        <th>Client id</th>
        <th>First name</th>
        <th>Last name</th>
        <th>Phone number</th>
        <th>Action</th>
        <c:forEach var="clientSearch" items="${requestScope.clientSearch}">          
            <tr>
              <td>${clientSearch.clientId}</td>
              <td class="td">${clientSearch.firstName}</td>
              <td class="td">${clientSearch.lastName}</td>
              <td>${clientSearch.phoneNumber}</td>
              <td><form action="./update_client" method="get">
                  <input type="hidden" name="clientId" value="${clientSearch.clientId}">
                  <input type="submit" value="Edit"/>
                </form></td>
            </tr>
        </c:forEach>
    </table>
    <script type="text/javascript">
      highlightTableRows("table", "clickedRow");
    </script>
  </c:if>
</div>
</body>
</html>