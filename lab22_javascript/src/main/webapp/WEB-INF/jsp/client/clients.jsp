<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
<script src="<c:url value="/resources/js/showHintForInputField.js"/>" ></script>
<script src="<c:url value="/resources/js/formValidation.js"/>" ></script>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<c:choose>
  <c:when test="${not empty requestScope.client}" >
    <title>Updating client</title>
  </c:when>
  <c:when test="${empty requestScope.client}" >
    <title>Creating client</title>
  </c:when>
</c:choose>
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <a href="./create_client">
    Add client
  </a>
  <a href="./find_client_by_name">
    Find client by name
  </a>
</div>
<div id="section">
  <c:choose>
    <c:when test="${not empty requestScope.client}" >
      <form action="./update_client" method="POST" onsubmit="return validate(this, {firstName : [REQUIRED_VALIDATOR, LETTERS_VALIDATOR], lastName : [REQUIRED_VALIDATOR, LETTERS_VALIDATOR], phoneNumber : [REQUIRED_VALIDATOR, NUMBERS_VALIDATOR]})">
        <fieldset>
          <legend>Edit client information : </legend>
          <table width="100%" cellspacing="0" cellpadding="4">
            <input type="hidden" name="clientId" value="${client.clientId}">
              <td width="40%"></br>
            <tr>
              <td align="right">First name : </td>
              <td width="60%"><input type="text" name="firstName" value="${client.firstName}" size="30"
                                 maxlength="64">
                                 <span id="hint">Edit clients first name</span>
                </br></td>
            </tr>
            <tr>
              <td align="right">Last name : </td>
              <td><input type="text" name="lastName" value="${client.lastName}" size="30"
                                 maxlength="64">
                                  <span id="hint">Edit clients last name</span>
                </br></td>
            </tr>
            <tr>
              <td align="right">Phone number : </td>
              <td><input type="text" name="phoneNumber" value="${client.phoneNumber}" size="30"
                                 maxlength="24">
                                  <span id="hint">Edit clients phone number</span>
                </br></td>
            </tr>
            <tr>
              <td></td>
              <td><input type="submit" value="Update"></td>
            </tr>
          </table>
        </fieldset>
      </form>
    </c:when>
    <c:when test="${empty requestScope.client}" >
      <form action="./create_client" method="POST" onsubmit="return validate(this, {firstName : [REQUIRED_VALIDATOR, LETTERS_VALIDATOR], lastName : [REQUIRED_VALIDATOR, LETTERS_VALIDATOR], phoneNumber : [REQUIRED_VALIDATOR, NUMBERS_VALIDATOR]})">
              <fieldset>
                <legend>Enter information about client : </legend>
                <table width="100%" cellspacing="0" cellpadding="4">
                  <tr>
                    <td width="40%" align="right">First name : </td>
                    <td width="60%"><input type="text" name="firstName" value="" size="30" maxlength="64">
                                            <span id="hint">Enter clients first name</span>
                      </br></td>
                  </tr>
                  <tr>
                    <td align="right">Last name : </td>
                    <td><input type="text" name="lastName" value="" size="30" maxlength="64">
                        					<span id="hint">Enter clients last name</span>
                      </br></td>
                  </tr>
                  <tr>
                    <td align="right">Phone number : </td>
                    <td><input type="text" name="phoneNumber" value="" size="30" maxlength="24">
                                            <span id="hint">Enter clients phone number</span>
                      </br></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td><input type="submit" value="Create"></td>
                  </tr>
                </table>
              </fieldset>
            </form>
    </c:when>
  </c:choose>
</div>
</body>
</html>