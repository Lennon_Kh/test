<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<script src="<c:url value="/resources/js/highlightTableRows.js"/>" ></script>
<style type="text/css">
  .clickedRow { background-color: #c1d879; }
</style>
<c:if test="${not empty requestScope.unfinishedOrders}">
  <title>Home</title>
</c:if>
<c:if test="${not empty requestScope.usersAndRoles}">
  <title>Users</title>
</c:if>
<c:if test="${not empty requestScope.listOfModels}">
  <title>Car models</title>
</c:if>
<c:if test="${not empty requestScope.listOfStatus}">
  <title>Order statuses</title>
</c:if>
<c:if test="${not empty requestScope.listOfCategories}">
  <title>Employee categories</title>
</c:if>
<c:if test="${not empty requestScope.listOfClients}">
  <title>Clients</title>
</c:if>
<c:if test="${not empty requestScope.listOfCars}">
  <title>Cars</title>
</c:if>
<c:if test="${not empty requestScope.listOfOrders}">
  <title>Orders</title>
</c:if>
<c:if test="${not empty requestScope.listOfEmployeeOrders}">
  <title>Orders for employee</title>
</c:if>
<c:if test="${not empty requestScope.listOfEmployees}">
  <title>Employees</title>
</c:if>
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<sec:authorize access="hasRole('ROLE_USER')">
<div id="nav-wrapper">
  <%@ include file="../html/menuForUser.html" %>
  </br>
</div>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_ADMIN')">
<div id="nav-wrapper">
  <%@ include file="../html/menu.html" %>
</div>
<div id="section">
  <c:if test="${not empty requestScope.usersAndRoles}">
    <a href="./create_user">
      Add user
    </a>
  </c:if>
  <c:if test="${not empty requestScope.listOfModels}">
    <a href="./create_car_model">
      Add car model
    </a>
  </c:if>
  <c:if test="${not empty requestScope.listOfStatus}">
    <a href="./create_order_status">
      Add order status
    </a>
  </c:if>
  <c:if test="${not empty requestScope.listOfCategories}">
    <a href="./create_emp_category">
      Add employee category
    </a>
  </c:if>
  <c:if test="${not empty requestScope.listOfClients}">
    <a href="./create_client">
      Add client
    </a>
    <a href="./find_client_by_name">
      Find client by name
    </a>
  </c:if>
  <c:if test="${not empty requestScope.listOfCars}">
    <a href="./create_car">
      Add car
    </a>
    <a href="./find_car_by_number">
      Find car by number
    </a>
  </c:if>
  <c:if test="${not empty requestScope.listOfOrders}">
    <a href="./create_order">
      Add order
    </a>
    <a href="./find_order_by_status">
      Find order by status
    </a>
  </c:if>
  <c:if test="${not empty requestScope.listOfEmployeeOrders}">
    <a href="./create_order_for_employee">
      Add order for employee
    </a>
  </c:if>
  <c:if test="${not empty requestScope.listOfEmployees}">
    <a href="./create_employee">
      Add employee
    </a>
  </c:if>
</div>
</sec:authorize>
<div id="section">
  <c:if test="${not empty requestScope.unfinishedOrders}">
    <jsp:include page="tableHomePage.jsp" flush="true" />
  </c:if>  
  <c:if test="${not empty requestScope.usersAndRoles}">
    <jsp:include page="user/tableUsersPage.jsp" flush="true" />
  </c:if>
  <c:if test="${not empty requestScope.listOfModels}">
    <jsp:include page="model/tableCarModelsPage.jsp" flush="true" />
  </c:if>
  <c:if test="${not empty requestScope.listOfStatus}">
    <jsp:include page="status/tableOrderStatusPage.jsp" flush="true" />
  </c:if>
  <c:if test="${not empty requestScope.listOfCategories}">
    <jsp:include page="category/tableEmpCategoryPage.jsp" flush="true" />
  </c:if>
  <c:if test="${not empty requestScope.listOfClients}">
    <jsp:include page="client/tableClientPage.jsp" flush="true" />
  </c:if>
  <c:if test="${not empty requestScope.listOfCars}">
    <jsp:include page="car/tableCarPage.jsp" flush="true" />
  </c:if>
  <c:if test="${not empty requestScope.listOfOrders}">
    <jsp:include page="order/tableOrderPage.jsp" flush="true" />
  </c:if>
  <c:if test="${not empty requestScope.listOfEmployeeOrders}">
    <jsp:include page="emplorder/tableOrderForEmpPage.jsp" flush="true" />
  </c:if>
  <c:if test="${not empty requestScope.listOfEmployees}">
    <jsp:include page="employee/tableEmployeePage.jsp" flush="true" />
  </c:if>
<script type="text/javascript">
  highlightTableRows("table", "clickedRow");
</script>
</div>
</body>
</html>
