<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>

Car models <br>
<br>
<table id="table" border=1 align=center>
  <tr>
    <th>Model id</th>
    <th>Name</th>
    <th>Action</th>
    <c:forEach var="listOfModels" items="${requestScope.listOfModels}">      
        <tr>
          <td>${listOfModels.carModelId}</td>
          <td class="td">${listOfModels.name}</td>
          <td><form action="./update_model" method="get">
              <input type="hidden" name="modelId" value="${listOfModels.carModelId}">
              <input type="submit" value="Edit"/>
            </form>
            <form action="./delete_model" method="get">
              <input type="hidden" name="modelId" value="${listOfModels.carModelId}">
              <input type="submit" value="Delete"/>
            </form></td>
        </tr>
    </c:forEach>
</table>
