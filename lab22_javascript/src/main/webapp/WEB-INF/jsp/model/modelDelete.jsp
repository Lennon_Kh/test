<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
<title>Deleting car model</title>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/resources/js/highlightTableRows.js"/>" ></script>
<style type="text/css">
  .clickedRow { background-color: #c1d879; }
</style>
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <c:if test="${not empty requestScope.modelToDelete}"> Are you sure you want to delete car  model? </c:if>
</div>
<div id="section">
  <c:if test="${empty requestScope.modelToDelete}"> Car model cant be deleted because
    it is used for description of car(s) !</br>
    </br>
    <a href="./car_models">
      Return to car models page
    </a>
  </c:if>
  <c:if test="${not empty requestScope.modelToDelete}">
    <table id="table" border=1 align=center>
      <tr>
        <th>Model id</th>
        <th>Name</th>
      <tr>
        <td>${modelToDelete.carModelId}</td>
        <td class="td">${modelToDelete.name}</td>
      </tr>
    </table>
  </c:if>
  </br>
  <table width="100%" cellspacing="0" cellpadding="4">
    <tr>
      <td align="right" width="50%"><c:if test="${not empty requestScope.modelToDelete}">
          <form method="POST" action="./delete_model">
            <input type="hidden" name="modelId" value="${modelToDelete.carModelId}">
            <input type="submit" value="Delete">
          </form>
        </c:if></td>
      <td width="50%"><c:if test="${not empty requestScope.modelToDelete}">
          <form action="./car_models">
            <input type="submit" value="Return">
          </form>
        </c:if></td>
    </tr>
  </table>
   <script type="text/javascript">
       highlightTableRows("table", "clickedRow");
     </script>
</div>
</body>
</html>