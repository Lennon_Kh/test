<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
<title>Car search</title>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<script src="<c:url value="/resources/js/showHintForInputField.js"/>" ></script>
<script src="<c:url value="/resources/js/highlightTableRows.js"/>" ></script>
<script src="<c:url value="/resources/js/formValidation.js"/>" ></script>
<style type="text/css">
  .clickedRow { background-color: #c1d879; }
</style>
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <a href="./create_car">
    Add car
  </a>
  <a href="./find_car_by_number">
    Find car by number
  </a>
</div>
<div id="section"> Enter cars number : </br>
  </br>
  <form action="./find_car_by_number" method="POST" onsubmit="return validate(this, {carNumber : [REQUIRED_VALIDATOR, ALPHANUMERIC_VALIDATOR]})">
    Number :
    <input type="text" name="carNumber" value="" size="30" maxlength="10">
    <span id="hint">Enter full number of a car</br>
    which you want to find</span>
    </br>
    </br>
    <input type="submit" value="Search">
  </form>
  <c:if test="${not empty requestScope.carSearch}">
    <br>
    <br>
    <table id="table" border=1 align=center>
      <tr>
        <th>Car id</th>
        <th>Car model</th>
        <th>Description</th>
        <th>Car number</th>
        <th>Client</th>
        <th>Action</th>
        <c:forEach var="carSearch" items="${requestScope.carSearch}">
            <tr>
              <td>${carSearch.carId}</td>
              <td>${carSearch.carModel.carModelId}</td>
              <td class="td">${carSearch.description}</td>
              <td>${carSearch.carNumber}</td>
              <td>${carSearch.client.firstName} ${carSearch.client.lastName}</td>
              <td><form action="./update_car" method="get">
                  <input type="hidden" name="carId" value="${carSearch.carId}">
                  <input type="submit" value="Edit"/>
                </form>
                  <form action="./delete_car" method="get">
                  <input type="hidden" name="carId" value="${carSearch.carId}">
                  <input type="submit" value="Delete"/>
                </form></td>
            </tr>
        </c:forEach>
    </table>
    <script type="text/javascript">
      highlightTableRows("table", "clickedRow");
    </script>
  </c:if>
</div>
</body>
</html>