<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
<c:choose>
  <c:when test="${not empty requestScope.car}" >
    <title>Updating car</title>
  </c:when>
  <c:when test="${empty requestScope.car}" >
    <title>Creating car</title>
  </c:when>
</c:choose>
<script src="<c:url value="/resources/js/showHintForInputField.js"/>" ></script>
<script src="<c:url value="/resources/js/formValidation.js"/>" ></script>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <a href="./create_car">
    Add car
  </a>
  <a href="./find_car_by_number">
    Find car by number
  </a>
</div>
<div id="section">
  <c:choose>
    <c:when test="${not empty requestScope.car}" >
      <form action="./update_car" method="POST" onsubmit="return validate(this, {description : [REQUIRED_VALIDATOR], carNumber : [REQUIRED_VALIDATOR, ALPHANUMERIC_VALIDATOR]})">
        <fieldset>
          <legend>Edit car information : </legend>
          <table width="100%" cellspacing="0" cellpadding="4">
            <input type="hidden" name="carId" value="${car.carId}">
              <td width="40%"></br>            
            <tr>
              <td align="right">Car model : </td>
              <td width="60%"><c:if test="${not empty requestScope.models}">
                  <select name="carModelId">
                      <option value="${car.carModel.carModelId}" selected>${car.carModel.name}</option>
                    <c:forEach var="models" items="${requestScope.models}">
                      <option value="${models.carModelId}">${models.name}</option>
                    </c:forEach>
                  </select>
                </c:if>
                <span id="hint">Choose the model of a car</span>
                </br></td>
            </tr>
            <tr>
              <td align="right">Description : </td>
              <td><textarea  maxlength="256" name="description" rows="7" cols="30">
						   ${car.description}</textarea>
						   <span id="hint">Enter description of a problem</br>
						   or any other required data</span>
                </br></td>
            </tr>
            <tr>
              <td align="right">Car number : </td>
              <td><input type="text" name="carNumber" value="${car.carNumber}" size="30" maxlength="10">
						    <span id="hint">Enter number of a car</span>
                </br></td>
            </tr>
            <tr>
              <td align="right">Clients last name : </td>
              <td><c:if test="${not empty requestScope.clients}">
                  <select name="clientId">
                      <option value="${car.client.clientId}"selected>${car.client.lastName}</option>
                    <c:forEach var="clients" items="${requestScope.clients}">
                      <option value="${clients.clientId}">${clients.lastName}</option>
                    </c:forEach>
                  </select>
                </c:if>
                <span id="hint">Choose the owner of a car</span>
                </br></td>
            </tr>
            <tr>
              <td></td>
              <td><input type="submit" value="Update"></td>
            </tr>
          </table>
        </fieldset>
      </form>
    </c:when>
    <c:when test="${empty requestScope.car}">
      <form action="./create_car" method="POST" onsubmit="return validate(this, {description : [REQUIRED_VALIDATOR], carNumber : [REQUIRED_VALIDATOR, ALPHANUMERIC_VALIDATOR]})">
        <fieldset>
          <legend>Enter information about car : </legend>
          <table width="100%" cellspacing="0" cellpadding="4">
            <tr>
              <td width="40%" align="right">Car model : </td>
              <td width="60%"><c:if test="${not empty requestScope.models}">
                  <select name="carModelId">
                    <c:forEach var="models" items="${requestScope.models}">
                      <option value="${models.carModelId}" selected>${models.name}</option>
                    </c:forEach>
                  </select>
                </c:if>
                <span id="hint">Edit the model of a car</span>
                </br></td>
            </tr>
            <tr>
              <td align="right">Description : </td>
              <td><textarea  maxlength="256" name="description" rows="7" cols="30">
						   </textarea>
						   <span id="hint">Edit description of the problem</br>
                           or any other data</span>
              </br></td>
            </tr>
            <tr>
              <td align="right">Car number : </td>
              <td><input type="text" name="carNumber" value="" size="30" maxlength="10">
						   <span id="hint">Edit number of the car</span>
                </br></td>
            </tr>
            <tr>
              <td align="right">Clients last name : </td>
              <td><c:if test="${not empty requestScope.clients}">
                  <select name="clientId">
                    <c:forEach var="clients" items="${requestScope.clients}">
                      <option value="${clients.clientId}">${clients.lastName}</option>
                    </c:forEach>
                  </select>
                </c:if>
                <span id="hint">Edit the owner of the car</span>
                </br>
                </br></td>
            </tr>
            <tr>
              <td></td>
              <td><input type="submit" value="Create"></td>
            </tr>
          </table>
        </fieldset>
      </form>
    </c:when>
  </c:choose>
</div>
</body>
</html>