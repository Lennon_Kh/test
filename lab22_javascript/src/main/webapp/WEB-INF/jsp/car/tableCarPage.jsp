<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>

Cars <br>
<br>
<table id="table" border=1 align=center>
  <tr>
    <th>Car id</th>
    <th>Car model</th>
    <th>Description</th>
    <th>Car number</th>
    <th>Client</th>
    <th>Action</th>
    <c:forEach var="listOfCars" items="${requestScope.listOfCars}">      
        <tr>
          <td>${listOfCars.carId}</td>
          <td class="td">${listOfCars.carModel.name}</td>
          <td class="td">${listOfCars.description}</td>
          <td>${listOfCars.carNumber}</td>
          <td class="td">${listOfCars.client.firstName} ${listOfCars.client.lastName}</td>
          <td><form action="./update_car" method="get">
              <input type="hidden" name="carId" value="${listOfCars.carId}">
              <input type="submit" value="Edit"/>
            </form>
            <form action="./delete_car" method="get">
              <input type="hidden" name="carId" value="${listOfCars.carId}">
              <input type="submit" value="Delete"/>
            </form></td>
        </tr>
    </c:forEach>
</table>
