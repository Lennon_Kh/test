<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
<title>Deleting car</title>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/resources/js/highlightTableRows.js"/>" ></script>
<style type="text/css">
  .clickedRow { background-color: #c1d879; }
</style>
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <c:if test="${not empty requestScope.carToDelete}"> Are you sure you want to delete car ?</c:if>
</div>
<div id="section">
  <c:if test="${empty requestScope.carToDelete}"> Car cant be deleted because it was
    added to car order(s) for repairing ! </br>
    </br>
    <a href="./cars">
      Return to cars page
    </a>
  </c:if>
  <c:if test="${not empty requestScope.carToDelete}">
    <table id="table" border=1 align=center>
      <tr>
        <th>Car id</th>
        <th>Car model</th>
        <th>Description</th>
        <th>Car number</th>
        <th>Client</th>
      <tr>
        <td>${carToDelete.carId}</td>
        <td class="td">${carToDelete.carModel.carModelId}</td>
        <td class="td">${carToDelete.description}</td>
        <td>${carToDelete.carNumber}</td>
        <td class="td">${carToDelete.client.firstName} ${carToDelete.client.lastName}</td>
      </tr>
    </table>
  </c:if>
  </br>
  <table width="100%" cellspacing="0" cellpadding="4">
    <tr>
      <td align="right" width="50%"><c:if test="${not empty requestScope.carToDelete}">
          <form method="POST" action="./delete_car">
            <input type="hidden" name="carId" value="${carToDelete.carId}">
            <input type="submit" value="Delete">
          </form>
        </c:if></td>
      <td width="50%"><c:if test="${not empty requestScope.carToDelete}">
          <form action="./cars">
            <input type="submit" value="Return">
          </form>
        </c:if></td>
    </tr>
  </table>
  <script type="text/javascript">
    highlightTableRows("table", "clickedRow");
  </script>
</div>
</body>
</html>