<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>

Unfinished orders <br>
<br>
<table id="table" border=1 align=center>
  <tr>
    <th>Car order id</th>
    <th>Status</th>
    <th>Order date</th>
    <th>Car number</th>
    <th>Description</th>
    <th>Action</th>
  </tr>
  <c:forEach var="unfinishedOrders" items="${requestScope.unfinishedOrders}">
    <tr>
      <td>${unfinishedOrders.carOrderId}</td>
      <td class="alt">${unfinishedOrders.statusName}</td>
      <td>${unfinishedOrders.carOrderStartDate}</td>
      <td class="alt">${unfinishedOrders.carNumber}</td>
      <td class="td">${unfinishedOrders.carDescription}</td>
      <td><form action="./update_order" method="get">
          <input type="hidden" name="carOrderId" value="${unfinishedOrders.carOrderId}">
          <input type="submit" value="Edit"/>
        </form>
        <c:if test="${unfinishedOrders.statusName eq 'new'}">
          <form action="./delete_order" method="post">
            <input type="hidden" name="carOrderId" value="${unfinishedOrders.carOrderId}">
            <input type="submit" value="Delete"/>
          </form>
        </c:if>
        <c:if test="${unfinishedOrders.statusName ne 'completed'}">
          <form action="./complete_order" method="get">
            <input type="hidden" name="carOrderId" value="${unfinishedOrders.carOrderId}">
            <input type="submit" value="Complete"/>
          </form>
        </c:if></td>
    </tr>
  </c:forEach>
</table>