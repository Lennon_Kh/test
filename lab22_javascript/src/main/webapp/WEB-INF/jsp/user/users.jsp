<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
<c:if test="${not empty requestScope.user}">
  <title>Updating user</title>
</c:if>
<c:if test="${not empty requestScope.roles1}">
  <title>Creating user</title>
</c:if>
<script src="<c:url value="/resources/js/showHintForInputField.js"/>" ></script>
<script src="<c:url value="/resources/js/formValidation.js"/>" ></script>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <a href="./create_user">
    Add user
  </a>
</div>
<div id="section">
  <c:if test="${not empty requestScope.user}">
    <form action="./update_user" method="POST" onsubmit="return validate(this, {login : [REQUIRED_VALIDATOR, ALPHANUMERIC_VALIDATOR], password : [REQUIRED_VALIDATOR, ALPHANUMERIC_VALIDATOR]})">
      <fieldset>
        <legend>Edit user information : </legend>
        <table width="100%" cellspacing="0" cellpadding="4">
          <input type="hidden" name="userId" value="${user.userId}">
            <td width="40%"></br>          
          <tr>
            <td align="right">User name(login) : </td>
            <td width="60%"><input type="text" name="login" value="${user.username}" size="30" maxlength="64">
                <span id="hint">Edit the name of the user</span>
              </br></td>
          </tr>
          <tr>
            <td align="right">Password : </td>
            <td><input type="text" name="password" value="${user.password}" size="30"  maxlength="64">
                <span id="hint">Edit the password of the user</span>
              </br></td>
          </tr>
          <tr>
            <td align="right">Role : </td>
            <td><c:if test="${not empty requestScope.roles}">
                <c:forEach var="roles" items="${requestScope.roles}">
                  <input type="radio" name="userRoleId" value="${roles.userRoleId}" checked>
                  ${roles.authority}</br>
                </c:forEach>
              </c:if>
              <span id="hint">Edit the role of the user</span>
              </br></td>
          </tr>
          <tr>
            <td></td>
            <td><input type="submit" value="Update"></td>
          </tr>
        </table>
      </fieldset>
    </form>
  </c:if>
  <c:if test="${not empty requestScope.roles1}">
    <form action="./create_user" method="POST" onsubmit="return validate(this, {login : [REQUIRED_VALIDATOR, ALPHANUMERIC_VALIDATOR], password : [REQUIRED_VALIDATOR, ALPHANUMERIC_VALIDATOR]})">
      <fieldset>
        <legend>Enter information about user : </legend>
        <table width="100%" cellspacing="0" cellpadding="4">
          <tr>
            <td width="40%" align="right">User name(login) : </td>
            <td width="60%"><input type="text" name="login" value="${user.login}" size="30" maxlength="64">
                    <span id="hint">Enter the name of a user</span>
              </br></td>
          </tr>
          <tr>
            <td align="right">Password : </td>
            <td><input type="text" name="password" value="${user.password}" size="30" maxlength="64">
                    <span id="hint">Enter a password of the user</span>
              </br></td>
          </tr>
          <tr>
            <td align="right">Role : </td>
            <td><c:forEach var="roles1" items="${requestScope.roles1}">
                <input type="radio" name="userRoleId" value="${roles1.userRoleId}" checked>
                ${roles1.authority}</br>
              </c:forEach>
              <span id="hint">Choose the role of the user</span>
              </br></td>
          </tr>
          <tr>
            <td></td>
            <td><input type="submit" value="Create"></td>
          </tr>
        </table>
      </fieldset>
    </form>
  </c:if>
</div>
</body>
</html>