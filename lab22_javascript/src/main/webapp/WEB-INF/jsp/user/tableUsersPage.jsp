<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>

Users <br>
<br>
<table id="table" border=1 align=center>
  <tr>
    <th>User id</th>
    <th>Login</th>
    <th>Password</th>
    <th>Role</th>
    <th>Action</th>
    <c:forEach var="usersAndRoles" items="${requestScope.usersAndRoles}">      
        <tr>
          <td>${usersAndRoles.userId}</td>
          <td >${usersAndRoles.username}</td>
          <td>${usersAndRoles.password}</td>
          <td>${usersAndRoles.userRole.authority}</td>
          <td><form action="./update_user" method="get">
              <input type="hidden" name="userId" value="${usersAndRoles.userId}">
              <input type="submit" value="Edit"/>
            </form>
              <form action="./delete_user" method="get">
              <input type="hidden" name="userId" value="${usersAndRoles.userId}">
              <input type="submit" value="Delete"/>
            </form></td>
        </tr>
    </c:forEach>
</table>
