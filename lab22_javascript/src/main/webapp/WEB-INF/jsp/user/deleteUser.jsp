<%@ taglib uri='http://java.sun.com/jstl/core_rt' prefix='c'%>
<html>
<head>
<title>Deleting user</title>
<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/resources/js/highlightTableRows.js"/>" ></script>
<style type="text/css">
  .clickedRow { background-color: #c1d879; }
</style>
</head>
<body>
<div id="header">
  <h1>Car Service Station</h1>
</div>
<div id="nav-wrapper">
  <%@ include file="../../html/menu.html" %>
</div>
<div id="section">
  <c:if test="${not empty requestScope.userToDelete}"> Are you sure you want to delete
    user ?</c:if>
</div>
<div id="section">
  <c:if test="${not empty requestScope.userToDelete}">
    <table id="table" border=1 align=center>
      <tr>
        <th>User id</th>
        <th>Login</th>
        <th>Password</th>
        <th>Role name</th>
      <tr>
        <td>${userToDelete.userId}</td>
        <td >${userToDelete.username}</td>
        <td>${userToDelete.password}</td>
        <td>${userToDelete.userRole.authority}</td>
      </tr>
    </table>
     <script type="text/javascript">
        highlightTableRows("table", "clickedRow");
     </script>
  </c:if>
  </br>
  <table width="100%" cellspacing="0" cellpadding="4">
    <tr>
      <td align="right" width="50%"><c:if test="${not empty requestScope.userToDelete}">
          <form method="POST" action="./delete_user">
            <input type="hidden" name="userId" value="${userToDelete.userId}">
            <input type="submit" value="Delete">
          </form>
        </c:if></td>
      <td width="50%"><c:if test="${not empty requestScope.userToDelete}">
          <form action="./users">
            <input type="submit" value="Return">
          </form>
        </c:if></td>
    </tr>
  </table>
</div>
</body>
</html>