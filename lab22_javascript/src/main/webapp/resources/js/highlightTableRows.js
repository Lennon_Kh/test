function highlightTableRows(tableId, clickClass) {

	var table = document.getElementById(tableId);

	if (clickClass) table.onclick = function(e) {
		if (!e) e = window.event;
		var elem = e.target || e.srcElement;
		while (!elem.tagName || !elem.tagName.match(/td|th|table/i)) elem = elem.parentNode;

		if (elem.parentNode.tagName == 'TR') {
			var clickClassReg = new RegExp("\\b"+clickClass+"\\b");
			var row = elem.parentNode;

			if (row.getAttribute('clickedRow')) {
				row.removeAttribute('clickedRow');
				row.className = row.className.replace(clickClassReg, "");
			} else {
				row.className += " "+clickClass;
				row.setAttribute('clickedRow', true);
			}
		}
	};
}