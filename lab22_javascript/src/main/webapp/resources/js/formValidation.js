var REQUIRED_VALIDATOR = {
	isValid : function(node) {
		return !!node.value;
	},
	message : "The field should not be empty!"
}

var LETTERS_VALIDATOR = {
    isValid : function(node) {
    	var letters = /^[a-zA-Z ]+$/;
    	return !!node.value.match(letters);
    },
    message : "The field should contain letters only!"
}

var NUMBERS_VALIDATOR = {
    isValid : function(node) {
    	var numbers = /^[0-9 ]+$/;
    	return !!node.value.match(numbers);
    },
    message : "The field should contain numbers only!"
}

var ALPHANUMERIC_VALIDATOR = {
    isValid : function(node) {
    	var alphanumeric = /^[0-9a-zA-Z ]+$/;
    	return !!node.value.match(alphanumeric);
    },
    message : "The field should contain numbers and letters only!"
}

var PRICE_VALIDATOR = {
	isValid : function(node) {
	    if (node.value.length > 0) {
		    return node.value > 0;
		} else {
            return true;
        }
	},
	message : "The price should be greater than zero!"
}

var CURRENT_DATE_VALIDATOR = {
    isValid : function(node) {
    	    var currentDate = Date.now();
    	    var enteredDate = Date.parse(node.value);
    	    return enteredDate <= currentDate;
    },
    message : "The registration date should be less or equal to current date and time!"
}

var END_DATE_VALIDATOR = {
    isValid : function(node) {
        if (node.value.length > 0) {
            	    var endDate = Date.parse(node.value);
            	    var startDate = Date.parse(node.parentNode.parentNode.parentNode.childNodes[4].lastChild.parentElement.childNodes[3].childNodes[1].value)
            	    return endDate >= startDate;
            	} else {
                    return true;
                }
    },
    message : "The completion date should be greater or equal to registration date and time!"
}

var COfE_END_DATE_VALIDATOR = {
    isValid : function(node) {
        if (node.value.length > 0) {
            	    var endDate = Date.parse(node.value);
            	    var startDate = Date.parse(node.parentNode.parentNode.parentNode.childNodes[5].lastChild.parentElement.childNodes[3].childNodes[1].value)
            	    return endDate >= startDate;
            	} else {
                    return true;
                }
    },
    message : "The completion date should be greater or equal to registration date and time!"
}



    function showError(container, errorMessage) {
    if (container.lastChild.className == "error-message") {
    } else {
      container.className = 'error';
      var msgElem = document.createElement('section');
      msgElem.className = "error-message";
      msgElem.innerHTML = errorMessage;
      container.appendChild(msgElem);
      }
    }

    function resetError(container) {
      container.className = '';
      if (container.lastChild.className == "error-message") {
        container.removeChild(container.lastChild);
      }
    }

function validate(form, fields) {
    var validationResult = true;
    for ( var i in fields) {
    	var requirements = fields[i];
    	for (j = 0; j < requirements.length; j++) {
    		var element = form.elements[i];
    		if(validationResult) {
    			resetError(element.parentNode);
    		}
    		if (!requirements[j].isValid(form.elements[i])) {
    			showError(element.parentNode, requirements[j].message);
    			validationResult = false;
    		}
    	}
    }
    return validationResult;
}
