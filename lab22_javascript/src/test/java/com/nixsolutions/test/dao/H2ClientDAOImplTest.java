package com.nixsolutions.test.dao;

import com.nixsolutions.dao.ClientDAO;
import com.nixsolutions.model.Client;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring.test.config.xml")
public class H2ClientDAOImplTest {
    @Autowired
    ClientDAO clientDAO;

    @Test
    public void shouldFindClientByIdInDB() {
        Client client = clientDAO.findById(3L);

        assertThat(client.getClientId(), is(3L));
        assertThat(client.getLastName(), is("Bonem"));
    }

    @Test
    public void shouldUpdateClientRecordInDB() {
        Client client = clientDAO.findById(4L);
        client.setFirstName("J.");

        clientDAO.updateClient(client);
        Client client1 = clientDAO.findById(4L);

        assertThat(client1.getFirstName(), is("J."));
        assertThat(client1.getLastName(), is("Frusciante"));
    }

    @Test
    public void shouldCreateClientRecordInDB() {
        Client client = new Client();
        client.setFirstName("J.");
        client.setLastName("F.K.");
        client.setPhoneNumber("0-800");

        clientDAO.createClient(client);

        assertThat(client.getFirstName(), is("J."));
        assertThat(client.getLastName(), is("F.K."));
        assertThat(client.getPhoneNumber(), is("0-800"));
    }

    @Test
    public void shouldDeleteClientRecordInDB() {
        clientDAO.deleteClient(clientDAO.findById(5L));

        assertThat(clientDAO.findById(5L), is(nullValue()));
    }

    @Test
    public void shouldFindAllClientRecordsInDB() {
        List<Client> list = clientDAO.findAllOrderById();

        assertThat(list.size(), is(4));
    }
}