package com.nixsolutions.test.dao;

import com.nixsolutions.dao.CarModelDAO;
import com.nixsolutions.model.CarModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring.test.config.xml")
public class H2CarModelDAOImplTest {
    @Autowired
    CarModelDAO carModelDAO;

    @Test
    public void shouldFindCarModelByIdInDB() {
        CarModel model = carModelDAO.findById(3L);
        CarModel model1 = carModelDAO.findById(5L);

        assertThat(model.getCarModelId(), is(3L));
        assertThat(model1.getCarModelId(), is(5L));
    }

    @Test
    public void shouldUpdateCarModelRecordInDB() {
        CarModel model = carModelDAO.findById(1L);
        model.setName("Volkswagen Caddy");

        carModelDAO.updateCarModel(model);
        CarModel model1 = carModelDAO.findById(1L);

        assertThat(model1.getName(), is("Volkswagen Caddy"));
    }

    @Test
    public void shouldDeleteCarModelRecordInDB() {
        carModelDAO.deleteCarModel(carModelDAO.findById(7L));

        assertThat(carModelDAO.findById(7L), is(nullValue()));
    }
}
