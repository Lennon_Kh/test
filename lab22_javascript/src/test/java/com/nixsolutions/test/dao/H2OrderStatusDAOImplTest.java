package com.nixsolutions.test.dao;

import com.nixsolutions.dao.OrderStatusDAO;
import com.nixsolutions.model.OrderStatus;
import org.hamcrest.core.AnyOf;
import org.hamcrest.core.StringStartsWith;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring.test.config.xml")
public class H2OrderStatusDAOImplTest {
    @Autowired
    OrderStatusDAO orderStatusDAO;

    @Test
    public void shouldFindOrderStatusByIdInDB() {
        OrderStatus status = orderStatusDAO.findById(2L);

        assertThat(status.getOrderStatusId(), is(2L));
    }

    @Test
    public void shouldCreateOrderStatusRecordInDB() {
        OrderStatus status = new OrderStatus("new2");
        orderStatusDAO.createOrderStatus(status);
        OrderStatus status1 = orderStatusDAO.findByName("new2");

        assertThat(status1.getName(), is("new2"));
    }

    @Test
    public void shouldFindOrderStatusByNameInDB() {
        OrderStatus status = orderStatusDAO.findByName("completed");

        assertThat(status.getName(), is("completed"));
    }

    @Test
    public void shouldUpdateOrderStatusRecordInDB() {
        OrderStatus status = orderStatusDAO.findById(1L);
        status.setName("unknown");

        orderStatusDAO.updateOrderStatus(status);
        OrderStatus status1 = orderStatusDAO.findById(1L);

        assertThat(status1.getOrderStatusId(), is(1L));
        assertThat(status1.getName(), is("unknown"));
    }

    @Test
    public void shouldFindAllOrderStatusRecordsInDB() {
        List<OrderStatus> list = orderStatusDAO.findAllOrderById();

        assertThat(list.size(), is(4));
    }
}
