package com.nixsolutions.test.dao;

import com.nixsolutions.dao.CarDAO;
import com.nixsolutions.dao.CarOrderDAO;
import com.nixsolutions.model.CarOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring.test.config.xml")
public class H2CarOrderDAOImplTest {
    @Autowired
    CarOrderDAO carOrderDAO;
    @Autowired
    CarDAO carDAO;

    @Test
    public void shouldFindCarOrderByIdInDB() {
        CarOrder order = carOrderDAO.findByIdIncludeCarAndStatus(3L);

        assertThat(order.getCarOrderId(), is(3L));
        assertThat(order.getOrderStatus().getOrderStatusId(), is(2L));
        assertThat(order.getPrice(), is(new BigDecimal("600.000")));
    }

    @Test
    public void shouldUpdateCarOrderRecordInDB() {
        CarOrder order = carOrderDAO.findById(1L);
        order.setPrice(new BigDecimal("1836.040"));
        order.setCar(carDAO.findById(3L));

        carOrderDAO.updateCarOrder(order);
        CarOrder order1 = carOrderDAO.findByIdIncludeCarAndStatus(1L);

        assertThat(order1.getCarOrderId(), is(1L));
        assertThat(order1.getOrderStatus().getOrderStatusId(), is(1L));
        assertThat(order1.getPrice(), is(new BigDecimal("1836.040")));
        assertThat(order1.getCar().getCarId(), is(3L));
    }

    @Test
    public void shouldDeleteCarOrderRecordInDB() {
        carOrderDAO.deleteCarOrder(carOrderDAO.findById(4L));

        assertThat(carOrderDAO.findById(4L), is(nullValue()));
    }
}
