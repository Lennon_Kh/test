package com.nixsolutions.test.dao;

import com.nixsolutions.dao.CarDAO;
import com.nixsolutions.dao.CarModelDAO;
import com.nixsolutions.model.Car;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring.test.config.xml")
public class H2CarDAOImplTest {
    @Autowired
    CarDAO carDAO;
    @Autowired
    CarModelDAO carModelDAO;

    @Test
    public void shouldFindCarByIdInDB() {
        Car car = carDAO.findByIdIncludeClientAndModel(2L);

        assertThat(car.getCarModel().getCarModelId(), is(6L));
        assertThat(car.getDescription(), is("Tune the engine"));
        assertThat(car.getCarNumber(), is("FW84208Q"));
    }

    @Test
    public void shouldUpdateCarRecordInDB() {
        Car car = carDAO.findById(1L);
        car.setCarModel(carModelDAO.findById(2L));

        carDAO.updateCar(car);
        Car car1 = carDAO.findByIdIncludeClientAndModel(1L);

        assertThat(car1.getCarModel().getCarModelId(), is(2L));
    }

    @Test
    public void shouldFindAllCarRecordsInDB() {
        List<Car> list = carDAO.findAllOrderById();

        assertThat(list.size(), is(4));
    }

    @Test
    public void shouldDeleteCarRecordInDB() {
        carDAO.deleteCar(carDAO.findById(4L));

        assertThat(carDAO.findById(4L), is(nullValue()));
    }
}
